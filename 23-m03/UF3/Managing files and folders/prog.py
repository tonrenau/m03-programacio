# /bin/python3
# Ton Renau
# EDT 1r ASIX
# Maig 2024
#  
# program to manage your system's file system
#------------------------------------------------------

import os
import subprocess
import fnmatch


def menuenty():
    """Print Menu 

    Args:
        NA

    Returns:
        options to menu 
    """
    print ("------------------------- MENU ------------------------\n")
    print ("1.  Display a folder's content")
    print ("2.  Create an empty file")
    print ("3.  Rename a file")
    print ("4.  Delete a file")
    print ("5.  Search for a file")
    print ("6.  Search for a file using wildcards")
    print ("7.  Search for files containing a particular string")
    print ("8.  Create a folder")
    print ("9.  Rename a folder")
    print ("10. Delete a folder")
    print ("11. Display file/folder permissions")
    print ("12. Exit \n")

def cleenScreen():
    """clean terminal for only see the menu 

    Args:
        NA 

    Returns:
        NA
    """
    if os.name == 'nt': # os.name == 'nt' : valida si el sistema es windows 
        os.system("cls")
    else:
        os.system("clear")

def validPath(path):
    """Fuction to validate the path of all the functions

    Args:
        Path : str 

    Returns:
        if path is incorrect returns exit
    """
    if not os.path.exists(path): # os.path.exists valida si path existeix
        print (f"Error: path {path} dosen't exist")
        exit
    return

def option_player():
    """ ask to the user one number to the menu

    Args:
         NA

    Returns:
        - Option : int 
        - Error msg
    """
    while True:
        try:
            option = int(input("Choose the option you want: "))
            if 1 <= option <= 12:    
                return option
            else:
                print (f"Incorrect option, the number {option} is not valid, please enter a number between 1 and 12.")
        except ValueError:
            print("Invalid input, please enter a valid number between 1 and 12. ")

def diplayFolderContents():
    """ print the content of the folder 

    Args:
        NA

    Returns:
        text
    """
    path=input(str("Introduce the path: "))
    validPath(path)
    try: 
     # detectar el sistema operatiu
        if os.name == "nt": 
            command = f'dir "{path}"'
        else:
            command = f'ls -l "{path}"'

        result = subprocess.run(command, shell=True, capture_output=True, text=True)
        print(result.stdout)
    
    except Exception as e:
        return f"Error: {e}"  
     
def craeteEmptyFile():
    """create a empty file in a folder  

    Args:
        NA

    Returns:
        - file 
        - Error msg
    """
    fileName = input(str("File name: "))
    with open(fileName, 'w') as fn:
        pass
    if os.path.exists(fileName):
        print (f"The file {fileName} has been created successfully")
    else:  
        print (f"Error: File could not be created ")

def renameFile():
    """ Function to reanme a file  

    Args:
        NA

    Returns:
        - txt 
        - Error msg 
    """
    oldFile = input(str("Enter the path of the file you want to rename:\n"))
    validPath(oldFile)
    newFile = input(str("Enter the same path and new name for the file:\n"))
    os.rename(oldFile, newFile)

    # verificar que a sigut renombrat 
    if os.path.exists(newFile) and not os.path.exists(oldFile):
        print (f"File has been renamed to '{newFile}' successfully")
    else:
        print (f"Error: File could not be renamed")

def deleteFile():
    """ Function to delete a file 

    Args:
        NA

    Returns:
        - txt
        - Error msg
    """
    path = input(str("Introduce the path: "))
    validPath(path)
    try:
        if os.name == "nt":
            command = f'del "{path}"'
        else:
            command = f'rm "{path}"'
        
        result = subprocess.run(command, shell=True, capture_output=True, text=True)

        if result.returncode == 0:
            print(f"'{path}' has been deleted successfully.")
        else:
            print(f"Error deleting '{path}': {result.stderr}")

    except PermissionError:
        return f'Error: you don''t have permissions to delete this file'
    except Exception as e:
        print(f"Unexpected error: {e}")

def searchFile():
    """ Function to search a file 

    Args:
        NA

    Returns:
        txt
    """
    path = input("Enter the directory path to search in:\n")
    filename = input("Enter the name of the file to search for:\n")
    
    validPath(path)
    
    found = False
    for root, dirs, files in os.walk(path): # os.walk recorra recusivament el dir i els subdirs 
        if filename in files:
            print(f"File '{filename}' found at: {os.path.join(root, filename)}")
            found = True
            break
    
    if not found:
        print(f"File '{filename}' not found in '{path}'")

def searchFileWildcards():
    """ Fucntion to search file with wildcards( * , ? ...)

    Args:
        NA

    Returns:
        txt
    """
    path = input("Enter the directory path to search in:\n")
    pattern = input("Enter the wildcard pattern to search for (e.g., *.txt, file?.doc):\n")
    
    validPath(path)
    
    found = False
    for root, dirs, files in os.walk(path):
        for filename in fnmatch.filter(files, pattern):
            print(f"File '{filename}' found at: {os.path.join(root, filename)}")
            found = True
    
    if not found:
        print(f"No files matching '{pattern}' found in '{path}'")

def searchFileString():
    """  function to search a file with only a string 

    Args:
        NA

    Returns:
        txt
    """
    path = input("Enter the directory path to search in:\n")
    search_string = input("Enter the string to search for within files:\n")
    
    validPath(path)
    
    found = False
    for root, dirs, files in os.walk(path):
        for filename in files:
            file_path = os.path.join(root, filename)
            try:
                with open(file_path, 'r', encoding='utf-8') as file:
                    if search_string in file.read():
                        print(f"String '{search_string}' found in file: {file_path}")
                        found = True
            except (UnicodeDecodeError, PermissionError) as e:
                print(f"Could not read file {file_path}: {e}")
    
    if not found:
        print(f"No files containing the string '{search_string}' found in '{path}'")

def createFolder():
    """Function to create a folder 

    Args:
        NA

    Returns:
        New folder
    """
    path = input(str("Introduce the path: "))
    
    try: 
        if os.name == "nt":
            command = f'mkdir "{path}"'
        else:
            command = f'mkdir -p "{path}"'
        
        result = subprocess.run(command, shell=True, capture_output=True, text=True)

        if result.returncode == 0:
            print(f"Directory '{path}' created successfully.")
        else:
            print(f"Error creating directory '{path}': {result.stderr}")

    except PermissionError:
        print("Error: You don't have permission to create this directory.")

def renameFolder():
    """ Fucntion to rename a folder

    Args:
        Na

    Returns:
        Change name 
    """
    oldir = input(str("Enter the path of the file you want to rename:\n"))
    validPath(oldir)
    newdir = input(str("Enter the same path and new name for the file:\n"))
    os.rename(oldir, newdir)

    # verificar que a sigut renombrat 
    if os.path.exists(newdir) and not os.path.exists(oldir):
        print (f"File has been renamed to '{newdir}' successfully")
    else:
        print (f"Error: File could not be renamed")

def deleteFolder():
    """ Function to delete a folder

    Args:
        NA

    Returns:
        delete folder
    """
    path = input(str("Introduce the path: "))
    validPath(path)

    try: 
        if os.name == "nt":
            command = f'rmdir /S /Q "{path}"'
        else: 
            command = f'rm -rf "{path}"'

        result = subprocess.run(command, shell=True, capture_output=True, text=True)
        if result.returncode == 0:
            print (f"Directory '{path}' has been deleted sucefully. ")
        else:
            print (f"Error: directory '{path}' hasn't been delated: {result.stderr}")

    except PermissionError:
        print("Error: You don't have permission to create this directory.")

def displayPermissions():
    """ function to give all permissions to file or folder 

    Args:
        NA

    Returns:
        txt 
    """
    path = input(str("Introduce the path: "))
    validPath(path)

    try:
        if os.name =="nt":
            command = f'icacls "{path}" /grant Todos:F /T'
        else:
            command = f'chmod 777 "{path}"'

        result = subprocess.run(command, shell=True, capture_output=True, text=True)
        if result.returncode == 0:
            print (f"The file/dir '{path}' has been change the permissions succefully. ")
        else:
            print (f"Error: file/dir '{path}' hasn't been change the permissions.")

    except PermissionError:
        print("Error: You don't have permission to change the permissions of this file or directory.")
    except Exception as e:
        print(f"Unexpected error: {e}")

def main():
    """ Function main

    Args:
        NA

    Returns:
        NA
    """
    while True:
        cleenScreen()
        menuenty()
        option = option_player()
        match option:
            case 1:
                diplayFolderContents()
            case 2:
                craeteEmptyFile()
            case 3:
                renameFile()
            case 4:
                deleteFile()
            case 5:
                searchFile()
            case 6:
                searchFileWildcards()
            case 7:
                searchFileString()
            case 8:
                createFolder()
            case 9:
                renameFolder()
            case 10:
                deleteFolder()
            case 11:
                displayPermissions()
            case 12:
                break
        input("Press Enter to continue...")



if __name__ == "__main__":
    main()