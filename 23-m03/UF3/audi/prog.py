# /bin/python3
# Ton Renau
# EDT 1r ASIX
# Abril 2024
#  
# Prgorama on sera  capaç de obrir i editar un archiu csv.
#------------------------------------------
import csv 
import os
from time import sleep

def show_menu():
    print ("1. Carregar dades")
    print ("2. Llistar dades")
    print ("3. Modificar Dades")
    print ("4. Desar dades")
    print ("5. Analitzar dades")
    print ("6. Sortir")

def get_option():
    choose = input("Escull una opció del menu: ")
    if choose > "6":
        print ("Escull un numero entre el 1 i el 6")
        return get_option()
    else:
        return choose

def download_csv():
    file = "./audi.csv"
    with open(file, newline='') as csv_file:
        data = list(csv.reader(csv_file)) # Emmegatzanem les dades en una llista 
        os.system("clear")
        print ("S'estan carregan les dades...")
        sleep(1.5)
        os.system("clear")
        print ("Dades carregades amb èxit")
        return data

def data_list(data):
    for row in data:
        print (row)

def modify_data(data):
    # Sol·licitar a l'usuari els canvis que desitja realitzar
    index_fila = int(input("Introdueix el número de fila que vols modificar (ultima linea del fitxer 10669): ")) - 1  # Restar 1 per convertir l'índex en base 1 a base 0
    index_columna = int(input("Introdueix el número de columna que vols modificar:\n 1:Model \n 2:Any \n 3:preu \n 4:Transmissió \n 5:Quilometratge \n 6:Tipus petroli \n 7:Taxa \n 8:Milles por galó \n 9:Mida motor ")) - 1  
    nou_valor = input("Introdueix el nou valor: ")

    # Modificar el valor en les dades
    try:
        data[index_fila][index_columna] = nou_valor
        print("Dades modificades correctament!")
    except IndexError:
        print("Error: L'índex de fila o columna està fora del rang.")


def save_data(data):
    file = "./audi.csv"
    with open(file, mode='w', newline='') as csv_file:
        writer = csv.writer(csv_file)
        writer.writerows(data)
    print("Dades desades correctament al fitxer CSV.")


def search_in_data(data):
    print("SEARCH")


def main():
    data = None  
    error_option = "Error: carrega primer les dades (opció 1)"
    while True:
        show_menu()
        choose = get_option()
        match choose:
            case "1":
                data = download_csv()
            case "2":
                if data is not None:  
                    data_list(data)
                else:
                    print(error_option)
            case "3":
                if data is not None:
                    modify_data(data)
                else:
                    print(error_option)
            case "4":
                if data is not None:
                    save_data(data)
                else:
                    print(error_option)
            case "5":
                if data is not None:
                    search_in_data(data)
                else:
                    print(error_option)
            case "6":
                break

if __name__ == '__main__':
    main()
