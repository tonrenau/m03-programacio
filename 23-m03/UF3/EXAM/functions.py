import os
import csv
import json
from time import sleep

def menuEntry():
    """ Funcio que imprimeix el menu

    Args:
        NA

    Returns:
        txt
    """
    print ("------------------------- MENU ------------------------\n")
    print ("1. Carregar clients")
    print ("2. Llistar clients")
    print ("3. Afegir client")
    print ("4. Eliminar client")
    print ("5. Modificar camp")
    print ("6. Eportar clients")
    print ("7. Generar estadístiques")
    print ("8. Sortir \n")

    return None

def menuEntryOption():
    """ Funcio que permet escollir quina opcio vol l'usuari del menu

    Args:
        NA

    Returns:
         1- option : int 
         2- Error msg
    """
    while True:
        try:
            option = int(input("Escull la opció del menu que vulguis: "))
            if 1 <= option <= 8:    
                return option
            else:
                print (f"Opcio incorrecte, el numero {option} no es valid, sisplau introdueix un numero entre 1 i 8.")
        except ValueError:
            print(f"Opcio incorrecte, sisplau introdueix un numero entre 1 i 8. ")

def carregarClie():
    """ Funcio que permet escollir quina opcio vol l'usuari del menuFormat

    Args:
        NA

    Returns:
         1- option : int 
         2- Error msg
    """
    menuFormat()
    option = menuFormatOption()
    match option:
        case 1:
            data = download_csv()
        case 2:
            data = download_json()
    return data

def menuFormat():
    """ Funcio que imprimeix el menu

    Args:
        NA
    """
    print ("1. Format .csv  (Format antic)")
    print ("2. Format .json (Format nou)")
    return None

def menuFormatOption():
    """ Funcio que permet escollir quina opcio vol l'usuari del menu

    Args:
        NA

    Returns:
         1- option : int 
         2- Error msg
    """
    while True:
        try:
            option = int(input("Escull la opció del menu que vulguis: "))
            if 1 <= option <= 2:    
                return option
            else:
                print (f"Opcio incorrecte, el numero {option} sisplau introdueix un numero entre 1 i 2.")
        except ValueError:
            print("Opcio incorrecte, sisplau introdueix un numero entre 1 i 2. ")

def download_csv():
    """ Funcio on descarrega les dades csv

    Args:
        NA

    Returns:
        data : biblioteca
    """
    try:
        file = "clients_wearfunck.csv"
        with open(file, newline='') as csv_file:
            data = list(csv.reader(csv_file)) # Emmegatzanem les dades en una llista 
            os.system("cls")
            print ("S'estan carregan les dades...")
            sleep(1.5)
            os.system("cls")
            print ("Dades carregades amb èxit")
            return data
    except PermissionError:
        return f'Error: you don''t have permissions to open this file'
    except Exception as e:
        print(f"Unexpected error: {e}")
    
def download_json():
    """ Funcio on descarrega les dades json

    Args:
        NA

    Returns:
        data : biblioteca
    """
    try:
        file = "clients_wearfunck.json"
        with open(file, "r") as json_file: 
            data = json.load(json_file)
            os.system("cls")
            print ("S'estan carregan les dades...")
            sleep(1.5)
            os.system("cls")
            print ("Dades carregades amb èxit")
        return data
    except PermissionError:
        return f'Error: you don''t have permissions to open this file'
    except Exception as e:
        print(f"Unexpected error: {e}")

def listClie(data):
    """ Funcio que llista cada linea del fitxer carregat

    Args:
        data : biblioteca

    Returns:
        None
    """
    for row in data:
        print(row)
    return None

def addClie(data):
    """ Funcio que afegiex un client

    Args:
        data : biblioteca

    Returns:
        
    """
    print ("Introdueix tots els valors del client: ")
    try: 
    
        new_client = {
            "NIF": input("NIF: "),
            "Name": input("Nom: "),
            "Surname": input("Cognom: "),
            "Age": int(input("Anys: ")),
            "Sex": input("Sexe: "),
            "Mail": input("Mail: "),
            "Spent": int(input("Gastat: ")),
            "Address": input("Address: "),
            "City": input("Ciutat: "),
            "Country": input("Pais: "),
        }

        for client in data:
            if client['NIF'] == new_client['NIF']:
                print("El DNI ja existeix. No es pot afegir el client")
                return data
        
        data.append(new_client)
        print("Client afegit amb exit")
        return data
    
    except Exception as e:
        print (f"No s'ha pogut afegir el usari, error: {e}")

def rmClie(data):
    """ Funcio que elimina un client 

    Args:
        data : biblioteca

    Returns:
        None
    """
    rmNif = input("Introdueix el NIF del client que vol eliminar: ")

    try:
        clie_found = False           
        for client in data: 
            if client['NIF'] == rmNif:
                clie_found = True
                data.remove(client)
                print("El client s'ha eliminat amb exit.")
                break
        if not clie_found:
            print("El client no s'ha trobat i no s'ha pogut eliminar, intenteu de nou.")
    except Exception as e:
        print ("S'ha produit un error: {e}")
    
    return data

def modClie(data):
    """ Funcio que modifica les dades de 1 client

    Args:
        data : biblioteca
    Returns:
        None
    """
    nif = input("Introdueix el NIF del client a modificar: ")
    client = next((client for client in data if client['NIF'] == nif), None)
    if client:
        field = input("Introdueix el camp a modificar: ")
        if field in client:
            client[field] = input(f"Introdueix el nou valor per a {field}: ")
            client['modified'] = True
            print("Camp modificat amb èxit.")
        else:
            print("Camp no trobat.")
    else:
        print("Client no trobat.")

    return data

def exportClie(data):
    filename = input("Introdueix el nom del fitxer per exportar (.json): ")
    try:
        with open(filename, mode='w', encoding='utf-8') as file:
            json.dump(data, file, indent=4)
        print(f"Clients exportats amb èxit a {filename}.")
    except Exception as e:
        print(f"Error exportant els clients: {e}")
    
def generteStatics(data):
    
    menuEntryStatics()
    option = menuOptionStatics()

    match option:
        case 1: 
            info_age(data)
        case 2:
            info_sex(data)
        case 3:
            info_spent(data)
            
    return None

def menuEntryStatics():
    print ("1. Informació de anys")
    print ("2. Informació de sexe")
    print ("3. informació de gastat")
    return None

def menuOptionStatics():
    while True:
            try:
                option = int(input("Escull la opció del menu que vulguis: "))
                if 1 <= option <= 3:    
                    return option
                else:
                    print (f"Opcio incorrecte, el numero {option} sisplau introdueix un numero entre 1 i 3.")
            except ValueError:
                print("Opcio incorrecte, sisplau introdueix un numero entre 1 i 3. ")

def info_age(data):    
    try:

        ages = [clie['Age'] for clie in data]
        
        minAge = min(ages)
        maxAge = max(ages)
        avgAge = sum(ages) / len(ages)
        
        print (f" - Maximum age: {maxAge}\n - Minimum age: {minAge}\n - Average age: {avgAge}")
    
    except Exception as e:
        print(f"Hi ha agut un error: {e}")

    return None

def info_sex(data):
    maleCount = 0 
    femaleCount = 0

    try:
        for sex in data:

            sexs = sex['Sex']
            
            if sexs == "Male":
                maleCount += 1
            elif sexs == "Female":
                femaleCount += 1

        print (f" - Males: {maleCount}\n - Females: {femaleCount}")

    except Exception as e:
        print(f"Hi ha agut un error: {e}")

    return None

def info_spent(data):

    try: 

        spents = [spent['Spent'] for spent in data]
        
        maxSpent = max(spents)
        minSpent = min(spents)
        totalSpent = sum(spents)
        avgSpent = totalSpent / len(spents)

        print (f" - Maximum spent: {maxSpent}\n - Minimum spent: {minSpent}\n - Total spent: {totalSpent}\n - Average spent: {avgSpent} ")

    except Exception as e:
        print(f"Hi ha agut un error: {e}")

    return None