# /bin/python3
# Ton Renau
# EDT 1r ASIX
# Maig 2024
#  
# Programa on gestiona l'empressa WearFunk Inc. amb tots els seus clients.
# --------------------------------------------------
import functions
import os
def main():
    """ Funcio main que fa de motor del programa

    Args:
        NA
    """
    data = None
    error_option = "Error: carrega primer les dades (opció 1)"
    while True:
        os.system("cls")
        functions.menuEntry()
        option = functions.menuEntryOption()
        match option:
            case 1: 
                data = functions.carregarClie()
            case 2:
                if data is not None:
                    functions.listClie(data)
                else: 
                    print(error_option)
            case 3:
                if data is not None:
                    functions.addClie(data)
                else:
                    print(error_option)
            case 4:
                if data is not None:
                    functions.rmClie(data)
                else:
                    print(error_option)
            case 5:
                if data is not None:
                    functions.modClie(data)
                else:
                    print(error_option)
            case 6:
                if data is not None:
                    functions.exportClie(data)
                else:
                    print(error_option)
            case 7:
                if data is not None:
                    functions.generteStatics(data)
                else:
                    print(error_option)
            case 8:
                break
        input("Premeu Enter per continuar...")

if __name__ == "__main__":
    main()

