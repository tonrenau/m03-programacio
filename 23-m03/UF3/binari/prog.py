# /bin/python3
# Ton Renau
# EDT 1r ASIX
# Maig 2024
#  
# codi binari a codi astri
#-------------------------------------------------------

def entry_menu():
    print("1. Descodificar codi binari")
    print("2. Codificar codi binari")
    print("3. Sortir")


def option_player():
    while True:
        option = int(input("Escull la opcio que vulguis: "))
        if 1 <= option <= 3:    
            return option
        else:
            print ("Opcio incorrecte, sisplau torna a introduir el num")

def open_file(path):
    try:
        with open(path, 'r') as file:
            data = file.read()
        return data
    except FileNotFoundError:
        return "El fitxer no s'ha trobat."
    except Exception as e:
        return f"Ocurrió un error: {e}"

def descodificar(data):
    text = ""
    for i in range(0, len(data), 8):
        byte = data[i:i+8]  # cada byte de 8 bits
        valor_decimal = int(byte, 2)   # byte binari a decimal
        caracter = chr(valor_decimal)  # valor decimal a caracter
        text += caracter
    print("Text descodificat:", text)

def codificar(data):
    codi_binari = ""
    for caracter in data:
        valor_ascii = ord(caracter)
        codi_binari += bin(valor_ascii)[2:].zfill(8)  # Afexeix 0 a la esquerra per asegurar 8 bits
    
    print ("Codi: ", codi_binari)



def main():
    path = "./binari/text.txt"
    while True:
        entry_menu()
        data = open_file(path)
        option = option_player()
        match option:
            case 1:
                descodificar(data)
            case 2: 
                codificar(data)
            case 3:
                break

if __name__ == "__main__":
    main()




# No em codifica i descodifica del tot correcte.