# /bin/python3
# Ton Renau
# EDT 1r ASIX
# Abril 2024
#  
# prog que llegeix un document json per depres filtrar la informacio.
#
# ----------------------------------------------------
import json
import random

def open_json(file_path):
    """Llegeix el json i el carrega com a biblioteca

    Returns:
        data: biblioteca
    """
    with open(file_path, "r") as file: # Obra el fixter json
        data = json.load(file) # descarrega en una variable el fitxer json

        return data
    
def random_animal(data):
    """De manera aleatoria s'escull un animal del fitxer

    Args:
        data (biblioteca): 

    Returns:
        animal: biblioteca 
    """
    animal = random.choice(list(data.keys())) # buscar en el json una entrada i escollirla de maenra aletoria
    return animal

def choose_option():
    """Demana al usuari escollir una opció

    Args:
        data (biblioteca): 

    Returns:
        choose (str)
    """

    print("\nAnimals of the world")
    print("\n1. Find out about an animal")
    print("2. Exit")
    choose = input("Enter your choice: ")

    return choose

def info_animal(data, animal, choose):
    """Imprimeix per pantalla tota la info del animal generat aleatoriament
      
      Args:
        data (biblioteca)
        animal (str)
        choose (str) 
    """

    match choose: # implementem un mtch case per triar l'opcio.
        case "1":# imprimeix la info del animal 
            print("Field 0:", animal.capitalize())
            print("Field 1:", data[animal]["field01"])
            print("Field 2:", data[animal]["field02"])
            print("Field 3:", data[animal]["field03"])
            print("Field 4:", data[animal]["field04"])
        case "2":
            print("Exiting the program.")
        case "3":
            print("Invalid option. Please enter 1 or 2.")


def main():
    data = open_json("/home/users/inf/hisx2/a221530tr/Documents/ASIX M03 UF2 exam files/ex01_untitled.json")
    animal = random_animal(data)
    while True: # fem un bucle perque el usuari acabi el programa quan ell vulgi.
        option = choose_option()
        if option == "2":
            break
        else:
            info_animal(data, animal, option)

if __name__=="__main__":
    main()