
import json

def open_json(file_path):
    """Llegeix el json i el carrega com a biblioteca

    Returns:
        data: biblioteca
    """
    with open(file_path, "r") as file: # Obra el fixter json
        data = json.load(file) # descarrega en una variable el fitxer json

        return data
    
def download_books():
    """Descaregar el fitxer json en una varable

    Returns:
        books: dictionary 
    """
    file_books = "/home/users/inf/hisx2/a221530tr/Documents/ASIX M03 UF2 exam files/ex4_Ton_Renau/books.json"
    books = open_json(file_books)
    return books

def menu_options():
    print ("1. Llistar llibres")
    print ("2. Reservar Libre")
    print ("3. Sortir")

    choose = input("\nClick your option: ")
    return choose

def print_books(data):
    """A menu that prints books

    Args:
        data (library)
    """
    
    count=1
    for option in data['books']:
        names = option['title'] 
        print (f"{count}: {names}")
        count += 1 
    print ("\n")
    return 