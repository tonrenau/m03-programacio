# /bin/python3
# Ton Renau
# EDT 1r ASIX
# Abril 2024
#  
# Programa que llegeix de un document json 

#--------------------------------------------------------------

import fileinput
import json # ERROR 1: No imporar el modul json.
def read_music_events_from_json(filename):
    with open(filename, "r" ) as file: # ERROR 2:Obrir el fitxer amb la "r" de read. 
        events = json.load(file)
    return events # ERROR 3: s'ha de rerunar el contigut de la variable on descrrega el contigut del fitxer json.

def mistery_function_a_part_1(events):
    if len(events) <= 1:
        return events
    
    mid = len(events) // 2
    left_half = events[:mid]
    right_half = events[mid:]

    left_half = mistery_function_a_part_1(left_half)
    right_half = mistery_function_a_part_1(right_half)

    return mistery_function_a_part_2(left_half, right_half)

def mistery_function_a_part_2(left_half, right_half):
    result = []
    left_index, right_index = 0, 0

    while left_index < len(left_half) and right_index < len(right_half):
        if left_half[left_index]["year"] < right_half[right_index]["year"]:
            result.append(left_half[left_index])
            left_index += 1
        elif left_half[left_index]["year"] > right_half[right_index]["year"]:
            result.append(right_half[right_index])
            right_index += 1
        else:
            result.append(left_half[left_index])
            result.append(right_half[right_index])
            left_index += 1
            right_index += 1

    result.extend(left_half[left_index:])
    result.extend(right_half[right_index:])
    return result

def mistery_function_b_part_1(events):
    i = 0
    while i < len(events)-1:
        index_start_year = i
        index_end_year = i
        while events[i]["year"] == events[index_end_year + 1]["year"]:
            index_end_year = index_end_year +1
        if index_start_year != index_end_year:
            events[index_start_year:index_end_year+1] = mistery_function_b_part_2(events[index_start_year:index_end_year+1])
            i = index_end_year
        i += 1
    return events

def mistery_function_b_part2(events):
    n = len(events)
    for i in range(n):
        swapped = False
        for j in range(0, n - i - 1):
            if events[j]["country"] > events[j + 1]["country"]:
                events[j], events[j + 1] = events[j + 1], events[j]
                swapped = True
        if not swapped:
            break

def show_final_event_list(events):
    print("\nMusic events in recent history\n")
    for event in events:
        print(f"{event['year']}  {event['country']} {event['event']}")
    # .ljust(12,".")
def main():
    filename = "/home/users/inf/hisx2/a221530tr/Documents/ASIX M03 UF2 exam files/ex03_music_events"
    # events =  
    print (read_music_events_from_json(filename))
    events_processed_by_function_a = mistery_function_a_part_1()
    events_processed_by_function_b = mistery_function_b_part_1(events_processed_by_function_a)
    show_final_event_list(events_processed_by_function_b)

if __name__ == "__main__":
    main()
