# ______________Apunts Funcions UF2 ____________________________


# -----Exercici de exemple------------------

# import math
# import sys 

# def is_prime(n):
#     """
#     checks ifa number is prime or not 
#     input: int
#     output: bool
#     """
#     divisor=2 
#     num_is_prime=True

#     while (divisor <= math.sqrt(n)) and num_is_prime:
#         if n % divisor == 0:
#             num_is_prime = False
#         divisor += 1
    
#     return num_is_prime

# limit = int(sys.argv[1])

# for num in range(2, limit): 
#     result = is_prime(num)
#     if result:
#         print(f"{num}")

# -----------------------------------------------------

# Programa amb proves de la funcio ------------------------

# import math

# def is_prime(n):
#     """
#     checks ifa number is prime or not 
#     input: int
#     output: bool
#     """
#     for i in range (2, int(math.sqrt(n)) + 1):
#         if n % i == 0:
#             return False
    
#     return True

# print (is_prime(2))
# print (is_prime(97))
# print (is_prime(100))