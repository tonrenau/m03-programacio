# /bin/python3
# Ton Renau
# EDT 1r ASIX
# Febrer 2024
#  
# Llegeix una quantitat d’hores, minuts i segons, i diu els segons totals.
# ------------------------------------------

def segons(hor, min, seg):
    """
    retorna els segons totals
    input: 3 int 
    output: int
    """
    seg_tot = (hor*3600) + (min*60) +seg
    return seg_tot

print (f" {segons(0, 0, 2)}", "segons")
print (f" {segons(0, 2, 0)}", "segons")
print (f" {segons(2, 0, 0)}", "segons")
print (f" {segons(2, 30, 30)}", "segons")