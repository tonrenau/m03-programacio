# /bin/python3
# Ton Renau
# EDT 1r ASIX
# Febrer 2024
#  
# Diu quants dies té un mes.
# -------------------------------------------

def days_month(month):
    """
    diu quans dies te un mes
    intput: int
    output: int
    """
    if month == (4 or 6 or 9 or 11):
        days = 30
    elif month == 2:
        days = 29
    else:
        days = 31

    return days



print (f"El mes,{2}, te {days_month(2)} dies")
print (f"El mes,{4}, te {days_month(4)} dies")
print (f"El mes,{5}, te {days_month(5)} dies")