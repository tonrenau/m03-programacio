# /bin/python3
# Ton Renau
# EDT 1r ASIX
# Febrer 2024
#  
# ------------------------------------
 
import math

def is_prime(n):
    """
    checks ifa number is prime or not 
    input: int
    output: boolean
    """
    if n < 0:
        return False
    else:
        for i in range (2, int(math.sqrt(n)) + 1):
            if n % i == 0:
                return False
        
        return True

print (is_prime(-7))
print (is_prime(2))
print (is_prime(7))
