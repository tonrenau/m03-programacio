# /bin/python3
# Ton Renau
# EDT 1r ASIX
# Febrer 2024
#  
# Rep la part numèrica d'un NIF i retorna la lletra
# -------------------------------------------

def lletra_dni(dni):
    """
    RETORNA la lletra del dni
    intput: int
    output: int
    """
    cadena = 'TRWAGMYFPDXBNJZSQVHLCKE'
    pocisio= dni % 23
    return cadena[pocisio]

num_dni=12345678
print (f"DNI:{num_dni}{lletra_dni(num_dni)}")


