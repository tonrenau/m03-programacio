# /bin/python3
# Ton Renau
# EDT 1r ASIX
# Febrer 2024
#  
# Fes un programa que desxifri missatges escrits en codi hexadecimal.
# Un missatge està escrit en codi hexadecimal quan s’ha substituït cada caràcter pel seu
# número de codi ASCII i aquest està representat amb dues xifres utilitzant la notació
# hexadecimal.
# Per exemple, el missatge ‘{}’ conté dos caràcters que corresponen als codis ASCII 123 i 125.
# Si passem aquests nombres a base 16, seran 7b i 7d, respectivament. O sigui, que si volem
# escriure ‘{}’ en hexadecimal cal posar la cadena 7b7d.
# La cadena hexadecimal s’ha de llegir per stdin, fent servir input(). Podeu redireccionar un
# fitxer.

# Missatge per traduir:
# 
# Pots fer un programa amb una funció que, donat un missatge, el xifri en codi hexadecimal?
# 2/2

# fet amb ajuda de chatgpt


def decodificar_hexadecimal(cadena_hex):
    mensaje = ""
    for i in range(0, len(cadena_hex), 2):
        byte_hex = cadena_hex[i:i+2]
        decimal = int(byte_hex, 16)
        caracter = chr(decimal)
        mensaje += caracter
    return mensaje

def main():
    cadena_hex = input("Introduce la cadena hexadecimal: ")
    mensaje = decodificar_hexadecimal(cadena_hex)
    print("Mensaje decodificado:", mensaje)

if __name__ == "__main__":
    main()

