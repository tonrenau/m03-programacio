# /bin/python3
# Ton Renau
# EDT 1r ASIX
# Febrer 2024
#  
# Diu si una persona és major d'edat o no.
# ------------------------------------------

def major(n):
    """
    validacio d'edat
    input: int
    output: boolean
    """
    if n >= 18:
        return True
    return False

print(major(5))
print(major(18))
print(major(33))


