# /bin/python3
# Ton Renau
# EDT 1r ASIX
# Febrer 2024
#  
# Diu si una cadena és un nombre sencer
# -------------------------------------------

def valida_num(cad):
    """
    validar si el la cadena es un numero
    input: str
    output: Boolean
    """
    if cad.isnumeric():
        return True
    else:
        return False 

print(valida_num("hola"))
print(valida_num("123"))
print(valida_num("ho781"))


