# /bin/python3
# Ton Renau
# EDT 1r ASIX
# Febrer 2024
#  
# Rep un NIF complet i el valida
# -------------------------------------------

def valida_DNI(dni):
    """
    Valida que el DNI tingui 8 num i que tingui 1 lletra que sigui la correcta
    input: string
    output: Boolean 
    """
    if dni[:-1].isnumeric():
        if len(dni[:-1]) == 8:
            valid = True
        else:
            valid = False
    else:
        valid = False

    if valid:
        lletra_dni = dni[-1]
        cadena = 'TRWAGMYFPDXBNJZSQVHLCKE'
        lletra = int(dni[:-1]) % 23 
        if cadena[lletra] == lletra_dni:
            valid = True
        else:
            valid = False
    else:
        valid = False

    return valid 




print (valida_DNI("47751439N"))
print (valida_DNI("12345678S"))
print (valida_DNI("12345678Z"))
print (valida_DNI("hsasd"))