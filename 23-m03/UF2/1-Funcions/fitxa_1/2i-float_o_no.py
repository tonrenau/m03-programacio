# /bin/python3
# Ton Renau
# EDT 1r ASIX
# Febrer 2024
#  
# Diu si una cadena és un float o no.
# -------------------------------------------



def float(cad):
    """
    validar si un num es float o no
    input:str
    output:float
    """

    if cad.isdigit():
        return True
    
    elif cad.count('.') == 1:        
        list_nums=cad.split('.')
        for nums in list_nums:
            if nums.isdigit():
                valor=True
        return valor
        
    else:
        return False 

print(float("12.a"))
print(float("12.4"))
print(float("12"))
print(float("12.12.12"))


