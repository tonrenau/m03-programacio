# /bin/python3
# Ton Renau
# EDT 1r ASIX
# Febrer 2024
#  
# Passa de segons al format ‘HH:MM:SS’
# -------------------------------------------

def hour(seg):
    """
    pasa de segons a hores
    intput: int
    output: int
    """
    hor = seg // 3600
    return hor

def minute(seg):
    """
    pasa de segons a minuts
    intput: int
    output: int
    """
    minut = seg % 3600 // 60
    return minut

def second(seg):
    """
    actualitza els segons
    intput: int
    output: int
    """
    seg_tot = seg % 60
    return seg_tot

segons=7203
print(f"{hour(segons):02d}:{minute(segons):02d}:{second(segons):02d}")