# /bin/python3
# Ton Renau
# EDT 1r ASIX
# Febrer 2024
#  
# Diu quants dies té un any.
# -------------------------------------------

def days_year(year):
    """
    diu els dies que te un any 
    intput: int
    output: int
    """
    if (year % 4 == 0 and year % 100 != 0) or (year % 400 == 0):
        days = 366
    else:
        days = 365 
        
    return days
    
print (days_year(1717))
print (days_year(2000))

