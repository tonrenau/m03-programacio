

#creem una llista establerta.
vectorselect = [52, 77, 63, 74, 18, 80, 92, 12]

def selectionsort(vectorselect):
    """Esta función ordenara el vector que le pases como argumento con el Método Selection Sort"""
    # Imprimimos la lista obtenida al principio (Desordenada)
    print ("El vector a ordenar es:",vectorselect)
    
    # largo = 0

    # for _ in vectorselect:
    #     largo += 1 # Obtenemos el largo del vector
    
    llargada = len(vectorselect) # descartem la seva opcio i fem amb el metode len 

    for i in range(llargada): 
      
        # Encontrar el minimo elemento de los restantes sin ordenar
        minimo = i # comença en 0 i va suman 
        for j in range(i+1, llargada): # la j sera el restrajador per canbiar el valor 
            if vectorselect[minimo] > vectorselect[j]: # compara el valor i amb tota la llista
                minimo = j # si troba un mes petit fa que sigui la j 
                
        # Cambiamos el elemento minimo encontrado con el primer elemento de la matriz
        vectorselect[i], vectorselect[minimo] = vectorselect[minimo], vectorselect[i]
        # Repetimos el proceso hasta terminar
    print("El vector ordenado es: ",vectorselect)

selectionsort(vectorselect)


