vectormerge = [52, 77, 63, 74, 18, 80, 92, 12]

def mergesort(vectormerge): 
    """Esta función ordenara el vector que le pases como argumento 
    con el Método Merge Sort"""
    
    # Imprimimos la lista obtenida al principio (Desordenada)
    print("El vector a ordenar con merge es:", vectormerge)
    
    def merge(vectormerge):
    
        def largo(vec):
            llargada = len(vec)
            return llargada
        
        
        if largo(vectormerge) >1: 
            medio = largo(vectormerge)//2 # Buscamos el medio del vector
            
            # Lo dividimos en 2 partes 
            izq = vectormerge[:medio]  
            der = vectormerge[medio:]

            # La funcio es crida a ella mateixa ja que a de fer el mateix procediment dos cops mes. 
            merge(izq) # Mismo procedimiento a la primer mitad
            merge(der) # Mismo procedimiento a la segunda mitad 
            
            i = j = k = 0
            
            # Copiamos los datos a los vectores temporales izq[] y der[] 
            while i < largo(izq) and j < largo(der): # miarem tots els valors de les dues llistes 
                if izq[i] < der[j]: # compara els dos valors seperats
                    vectormerge[k] = izq[i] # si el de l'esquera es mes petit es fica en la pocisio de la k 
                    i+= 1
                else: 
                    vectormerge[k] = der[j] # el mateix per siu es el element dret
                    j+= 1
                k += 1
            
            # Nos fijamos si quedaron elementos en la lista
            # tanto derecha como izquierda 
            while i < largo(izq): 
                vectormerge[k] = izq[i] 
                i+= 1
                k+= 1
            
            while j < largo(der): 
                vectormerge[k] = der[j] 
                j+= 1
                k+= 1
    merge(vectormerge)
    print("El vector ordenado con merge es: ", vectormerge)
mergesort(vectormerge)