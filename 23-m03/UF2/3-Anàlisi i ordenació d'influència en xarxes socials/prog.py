# /bin/python3
# Ton Renau
# EDT 1r ASIX
# Abril 2024
#  
# Implementar una funció per llegir el fitxer d'entrada i emmagatzemar 
# les dades en una estructura adequada.
# -----------------------------------------------------------------

import json

def open_json(file_path):
    """obra un archiu json com una biblioteca

    Args:
        file_path (doc): document 

    Returns:
        data: documentacio del json
    """
    with open(file_path, "r") as file: # open file JSON
        data = json.load(file) # download data JSON

        return data

def process_result(data):
    """Processar les dades de la bibliteca donada

    Args:
        result (biblioteca)
    """
    # for user in result:
    user_grades_total = {}
    for item in data:
        user_id = item['user_id']
        publications = item['publications']
         
        total_grade = 0
        num_grades = 0
        
        # calcular la suma de les grades amb cada user
        for pub in publications:
            total_grade += pub['grade']
            num_grades += 1

        # calcular la infuencia
        if num_grades >0 :
            avrg_grade = total_grade / num_grades
        else :
            avrg_grade = 0

        user_grades_total[user_id] = avrg_grade

    return user_grades_total

def quicksort(arr):
    if len(arr) <= 1:
        return arr
    pivot = arr[len(arr) // 2][1]
    left = [x for x in arr if x[1] > pivot]
    middle = [x for x in arr if x[1] == pivot]
    right = [x for x in arr if x[1] < pivot]
    return quicksort(left) + middle + quicksort(right)

if __name__ == "__main__":
    file_path = "/home/users/inf/hisx2/a221530tr/m03-programacio/23-m03/UF2/3-Anàlisi i ordenació d'influència en xarxes socials/file.json" # path to file JSON
    data = open_json(file_path) # agafa tota la biblioteca del arxiu json
    result = process_result(data)
    result_list = [(key, value) for key, value in result.items()] # convertir el diccionari en una llista de tuplas per utilitzar el quicksort
    sort_result = quicksort(result_list)

    print(sort_result)
