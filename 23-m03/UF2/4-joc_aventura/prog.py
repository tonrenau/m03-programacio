# /bin/python3
# Ton Renau
# EDT 1r ASIX
# Abril 2024
#  
# Crear un joc a traves de varis arxius json.
# ----------------------------------------------------------

import json
import random
import os 
from time import sleep

def open_json(file_path):
    """obra un archiu json com una biblioteca

    Args:
        file_path (doc): document 

    Returns:
        data: documentacio del json
    """
    with open(file_path, "r") as file: # open file JSON
        data = json.load(file) # download data JSON

        return data

def download_characters():
    #file_characters = "/home/users/inf/hisx2/a221530tr/m03-programacio/23-m03/UF2/4-joc_aventura/characters.json"# path to file JSON
    file_characters = "/home/trenau/m03-programacio/23-m03/UF2/4-joc_aventura/characters.json"
    characters = open_json(file_characters)
    return characters
def download_locations():
   # file_locations = "/home/users/inf/hisx2/a221530tr/m03-programacio/23-m03/UF2/4-joc_aventura/locations.json"
    file_locations = "/home/trenau/m03-programacio/23-m03/UF2/4-joc_aventura/locations.json"
    locations = open_json(file_locations)
    return locations
def download_items():
    #file_items = "/home/users/inf/hisx2/a221530tr/m03-programacio/23-m03/UF2/4-joc_aventura/items.json"
    file_items = "/home/trenau/m03-programacio/23-m03/UF2/4-joc_aventura/items.json"
    items = open_json(file_items)
    return items
def download_encounters():
    #file_encounters = "/home/users/inf/hisx2/a221530tr/m03-programacio/23-m03/UF2/4-joc_aventura/encounters.json"
    file_encounters = "/home/trenau/m03-programacio/23-m03/UF2/4-joc_aventura/encounters.json"
    encounters = open_json(file_encounters)
    return encounters

def option_player():
    """With the case method, the user can choose an option

    Args:
        choose (int)

    Returns:
        num (int)
    """
    choose = input("Choose your option: ")   
    # match choose:
    #     case "1": num = "1"
    #     case "2": num = "2"
    #     case "3": num = "3"
    return choose

def print_options(data):
    """A menu that prints places

    Args:
        data (library)
    """
    
    count=1
    for option in data['options']:
        names = option['name'] 
        print (f"{count}: {names}")
        count += 1 
    print ("\n")
    return 

def descipcion_place(num, locations):
    """with case option, print her description

    Args:
        num (str)
        locations (library):

    Returns:
        _type_: _description_
    """
    data = locations['options'] 
    if num == "1":
        place = "Dark Forest"   
    elif num == "2":
        place = "Peaceful Village" 
    for option in data:
        if option['name'] == place:
            description = option['description']
        
    return description
    
def random_encounters(encounters):
    print ("Oh no, you meet a monster, fight it. \n")
    data = encounters['options']
    enemies = random.choice(data)
    print (f"{enemies['name']} \n {enemies['description']} \n ")
    return enemies

def  battle(enemie):
    if enemie['name'] == 'Dragon':
        sleep(1.5)
        print("You are death")
        sleep(1.5)
        os.system("clear")
        print("GAME OVER")
        num = 1
    else:
        sleep(1.5)
        print("You won the battle")

        num = 0
    return num

    
    
def choose_item(num,items):
    """with case option, print her description

    Args:
        num (str):
        items (library): 

    Returns:
        _type_: _description_
    """
    data = items['options']
    if num == "1":
        weapon = "Sword"
    elif num == "2":
        weapon = "Potion"
    for option in data:
        if option['name'] == weapon:
            description = option['description']
    return description
    



def main():
    print ("Welcome to Vorthal Island.")
    print ("Your mission is to explore all the areas of our country, but be careful, you may have encounters that are not very friendly. \n")
    characters = download_characters()
    locations = download_locations()
    items = download_items()
    encounters = download_encounters()
    while True:
        # Location
        print ("You can choose an area to explore \n")
        print_options(locations)
        option = option_player()
        if option == "2":
            print ("Happy place to recover energy and prepare for your battle. \n")
            sleep(3)
            os.system("clear")
        elif option >= "3":
            break
        else: 
            print(f"{descipcion_place(option, locations)}\n")
            sleep(2.5)
            os.system("clear")
            # Encounter
            enemie = random_encounters(encounters)
            # battle weapon
            print ("Prepare for battle, choose your weapon: \n")
            print (f"{print_options(items)}\n")
            option = option_player()
            if option >= "3":
                break
            print (choose_item(option,items))
            sleep(2.5)
            os.system("clear")
            # decicde battle 
            num = battle(enemie)
            if num == 1:
                break 
            elif num == 0: 
                sleep(2.5)
                os.system("clear")
                print("Continue with your adventure")
            
            


if __name__ == "__main__":
   main()


