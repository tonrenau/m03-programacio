# /bin/python3
# Ton Renau
# EDT 1r ASIX
# Març 2024
#  
# Implementa un programa que calculi la freqüència de cada vocal en un text, i ens mostri
# l'histograma horitzontal.
# I si en comptes de comptar la freqüència de les vocals, volem l'histograma de les lletres 'r',
# 'p' i 'q'?
# I si volem saber la freqüència de les paraules 'hola' , 'adeu', 'andreu'?
# Quin problema hi ha amb l'histograma horitzontal de les vocals? Justifica fer una funció
# diferent?
# Si ja has creat el programa que gestiona les vocals en un text, millora'l per acomodar els
# nous requisits, i assegura't de revisar el docstring de les funcions on facis canvis.
# 2/2
# ---------------------------------------------------------------


text = "Hola bon dia a tothom"

def convert_text_to_array(text):
    array_text = text.split()
    return array_text




def main():
    print (convert_text_to_array(text))

if __name__=="__main__":
    main()