import is_num as num


numbres=[]
strings=[]

# seperar arg en dos arrays

def separete_arg(arg):
    """
    seperar a dos arrays els tipus de cada argument 
    input: arg
    output: 2 arrays
    """
    for valor in arg:
        if num.is_num(valor):
            numbres.append(valor)
        else:
            strings.append(valor)
    return 0

# identificar el valor max de cada array
def max_valor(array):
    """
    get max valor that one's array
    input:array
    output: string
    """
    return max(array)


def main():
    
    separete_arg(["hello", "123", "apple", "45.3"])
    print(numbres)
    print(strings)
    print("-------------------------")
    print(max_valor(numbres))
    print(max_valor(strings))
    return 0


if __name__ == "__main__":
    main()

