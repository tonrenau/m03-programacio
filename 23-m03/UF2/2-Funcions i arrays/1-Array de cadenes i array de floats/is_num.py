# /bin/python3
# Ton Renau
# EDT 1r ASIX
# Març 2024
#  
# Implementa un programa que rep diversos arguments, distingeix els que representen una
# dada numèrica dels que representen una cadena de text, crea un array de floats i un altre de
# cadenes, i mostra el màxim valor de cada un d'aquests arrays.
#
# Exemple d’execució:
# separate_numbers_and_strings.py hola 6 45.5 com estas 0 adeu 12
# Ha de mostrar:
# Maximum number: 45.5
# Maximum string: hola
# Què és el màxim d’un array de cadenes? Respon aquesta pregunta a la capçalera del
# programa.
# ----------------------------------------------------------



def is_num(string):

    return string.lstrip("+-").replace(".", "", 1).isdigit()

def main():
    print(is_num("63"))
    print(is_num("+63"))
    print(is_num("hello"))
    print(is_num("63.3"))
    print(is_num("63.3.4"))
    print(is_num("63a"))
    return 0



if __name__ == "__main__":
    main()



