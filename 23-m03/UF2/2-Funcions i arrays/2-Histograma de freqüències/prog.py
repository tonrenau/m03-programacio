# /bin/python3
# Ton Renau
# EDT 1r ASIX
# Març 2024
#  
# implementa un programa que rep el nom d'un fitxer de text com argument, llegeix el seu
# contingut, i genera un histograma horitzontal amb la freqüència de les longituds de les
# paraules del text.
#
# Si suposem que input.txt conté el text bon dia a tothom, l'execució ha de mostrar:
# 1: #
# 2:
# 3: ##
# 4:
# 5:
# 6: #
#------------------------------------------------------


def file_into_array(file):
    """
    obrir el fixer i convertir el text en array
    input; file 
    output: array
    """
    with open(file , "r" ) as word:
        text = word.read()
        array_words = text.split()
    return array_words

def length_word(array_words):
    """
    Diu la llargada de cada paraula de l'array
    input: array
    output; array
    """
    long_word = [len(word) for word in array_words]
    return long_word

def generate_histograma(array_words):
    """
    generem uun diccionari de l'histograma a partir de una array
    input: array
    output: diccionary
    """
    histo = {}
    for word in array_words:
        length = len(word)
        if length in histo:
            histo[length] += 1
        else: 
            histo[length] = 1
    return histo

def print_histograma(histogrma, long_word):
     """
     dibuixa l'histogrma de manera adequada 
     input: diccionary i array 
     output: print 
     """
     max_num = max(long_word)
     for length in range(1, max_num +1):
        freq = histogrma.get(length, 0) # el .get agafa el valor assosiatiu del diccionari.
        print (f"{length}: {'#' * freq}")
     



def main():
    array = file_into_array("/home/users/inf/hisx2/a221530tr/m03-programacio/.../file.txt")    
    long_word = length_word(array)
    histograma = generate_histograma(array)
    print (print_histograma(histograma, long_word))
    

if __name__ == "__main__":
    main()


