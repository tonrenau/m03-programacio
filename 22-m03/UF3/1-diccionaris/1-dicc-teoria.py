# DEFINIR DICCIONARIS:
# diccionario = {"clave1":"valor1", "clave2":"valor2"}
cotxes = {"Ford":"Fiesta", "Seat":"Leon"}

# ---------------------------------------------------------------------------------

# ACCEDIR ALS ELEMENTS DEL DICCIONARI:
# print(diccionario["clave1"])
print(cotxes["Ford"])   # Retorna "Fiesta"
print(cotxes["Seat"])   # Retorna "Leon"

# ---------------------------------------------------------------------------------

# AFEGIR ELEMENTS AL DICCIONARI:
# diccionario["clave1"] = "valor1"
cotxes["Porsche"] = "Panamera"
print(cotxes) # {'Ford': 'Fiesta', 'Seat': 'Leon', 'Porsche': 'Panamera'}
# Si ja existeix diccionario["clave1"] es modificara el seu contingut

# ---------------------------------------------------------------------------------

# ELIMINAR ELEMENTS DEL DICCIONARI:
# diccionario.pop("clave2")
cotxes.pop("Seat")
print(cotxes) #{'Ford': 'Fiesta', 'Porsche': 'Panamera'}

# Si intentes eliminar un element que no es troba al diccionari retorna un error
# a no ser que indiquis un valor per defecte:
cotxes.pop("clave5", "error")

# ---------------------------------------------------------------------------------

# ELIMINAR ULTIM ELEMENT DEL DICCIONARI:
# diccionario.popitem()
cotxes.popitem()
print(cotxes) # {'Ford': 'Fiesta'}

# ---------------------------------------------------------------------------------

# MODIFICAR ELEMENTS DEL DICCIONARI:
# Metode 1: diccionario.update({"clave1": "valor2})
cotxes.update({"Ford": "Focus"})
print(cotxes)

# Metode 2: diccionario["clave1"] = "valor3"
cotxes["Ford"] = "Mustang"
print(cotxes)

# ---------------------------------------------------------------------------------

# ORDENAR UN DICCIONARI:
# Segons el la clau:
# sorted(d.items(), key=lambda k:k[0])
sorted(cotxes.items(), key=lambda k:k[0])

# Segons el valor:
# sorted(d.items(), key=lambda k:k[1])
sorted(cotxes.items(), key=lambda k:k[1])



