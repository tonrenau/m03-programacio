# Repte 3: Desenvolupeu un programa que determini si un fitxer es troba (o existeix) en un
# directori donat o no. Si el fitxer existeix, llegir-hi les primeres cinc línies. Si el fitxer no
# existeix, demanar a l’usuari un nom per al fitxer i crear-lo en el directori corresponent.

# Important: Utilitzeu l’estructura try: / except: per a realitzar el programa. Us podeu
# documentar al següent enllaç: https://docs.python.org/3/tutorial/errors.html

from sys import argv

try:
    with open(argv[1], 'w') as file
    

