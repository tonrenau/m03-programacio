# Repte 5: Creeu un fitxer anomenat “Usuaris.txt” i creeu-hi cinc usuaris ficticis seguint
# l’estructura següent: Nom_usuari (Indiqueu el nom i el cognom separats per un “_”, per
# exemple: Joan_Pérez), Contrasenya (que estigui composta per lletres i números, per
# exemple: C1ont4), Edat (l’edat en anys de l’usuari, per exemple: 34), Administrador (una
# variable booleana “SI o NO” que determina si l’usuari és administrador o no). Cada
# usuari correspon a una fila i les seves dades estan separades per comes “,”.
# Desenvolupeu un programa que llegeixi aquest document, introduint-hi el directori
# complet que correspon a aquest fitxer i que, en el mateix directori creï un fitxer anomenat
# M14 _ Python 2HSIX 2022-23
# “Llista_usuaris.txt”. Aquest nou fitxer ha de contenir quatre línies: La primera comença
# amb Noms: seguit de tots els noms d’usuari del fitxer “Usuaris.txt”, separant el nom de
# cada usuari amb una coma “,”. La segona línia comença amb Contrasenyes: , la tercera
# amb Edats: i la quarta amb Administradors: , seguint la mateixa estructura que presenta
# la línia Noms.
# Si el fitxer “Usuaris.txt” no existeix, el programa ha d’informar a l’usuari
# d’aquest fet sense que es generi un error que aturi l’execució d’aquest
# Exemple: Si el fitxer “Usuaris.txt” fos el següent:
# Ariadna_pla, 80trwl, 57, SI
# Joan_Martínezm, C1ont45, 34, NO
# Llavors, el fitxer “Llista_usuaris.txt” serà el següent:
# Noms: Ariadna_pla, Joan_Martínez
# Contrasenyes: 80trwl, C1ont45
# Edats: 57, 34
# Administradors: SI, NO




