# - Repte 6: Realitza un programa que executi el següent algorisme:
# Pas 1. Demanar a l’usuari que introdueixi el nom d’un fitxer (precedit per la seva direcció si
# el fitxer es troba fora del directori on està guardat l’script que estem executant). Saltar al
# Pas 2.
# Pas 2. Si el que ha introduït l’usuari és un directori, en comptes d’un fitxer, el programa
# retorna a l’usuari un avís indicant-li que ha introduït un directori i li comunica que escrigui
# “Sortir” si vol aturar el programa o que escrigui la direcció d’un fitxer. Si el que ha introduït
# l’usuari és un fitxer que no existeix, s’avisa a l’usuari d’aquest fet i se li comunica que
# escrigui “Sortir” si vol aturar el programa o que escrigui la direcció d’un fitxer existent. Si el
# que ha introduït l’usuari és un fitxer existent, saltar al Pas 3.
# Pas 3. Si el programa no és capaç de llegir el fitxer (explora els casos en que un fitxer
# determinat no es pot llegir*), indicar a l’usuari que el fitxer que ha introduït no es pot llegir i
# que escrigui “Sortir” si vol aturar el programa o que escrigui la direcció d’un nou fitxer. Si el
# programa pot llegir el contingut del fitxer, saltar al Pas 4.
# *Per exemple, se’ns poden presentar problemes si intentem llegir una imatge. També, per
# exemple, se’ns poden presentar problemes amb el format de codificació de caràcters que
# utilitza el fitxer
# En el següent enllaç trobaràs informació per a realitzar un primer contacte amb els formats
# de codificació de caràcters a l’hora de llegir fitxers amb python:
# https://melaniewalsh.github.io/Intro-Cultural-Analytics/02-Python/07-Files-Character-Encodi
# ng.html
# Pas 4. Demanar a l’usuari que introdueixi una paraula clau. Si dins del contingut del fitxer
# no es troba la paraula introduïda, indicar a l’usuari que no existeix cap seguit de caràcters
# en el fitxer que corresponguin amb la paraula clau i comunicar-li que escrigui “Sortir” si vol
# aturar el programa o que escrigui una altra paraula clau. Si dins del contingut del fitxer es
# troba la paraula introduïda, saltar al Pas 5.
# Pas 5. Indicar a l’usuari el número de línies que s’han trobat en el fitxer, on la paraula clau
# hi apareix. Comunicar a l’usuari que a continuació es crearà un nou fitxer que tindrà com a
# contingut les línies del fitxer original on hi ha la paraula clau. Saltar al Pas 6.
# Pas 6. Demanar a l’usuari que escrigui la direcció on vol guardar el nou fitxer. Si la direcció
# no existeix o no és vàlida, indicar-ho a l’usuari i demanar-li que escrigui “Sortir” si vol aturar
# el programa o que escrigui una direcció vàlida. Si la direcció existeix i és vàlida, demanar a
# l’usuari que escrigui el nom del nou fitxer. Saltar al Pas 7.
# Pas 7. Crear el nou fitxer i guardar-lo a la direcció i amb el nom indicats per l’usuari.
# Comunicar a l’usuari que el fitxer s’ha creat amb èxit. Saltar al Pas 8.
# Pas 8. Mostrar per pantalla el contingut del nou fitxer. Sortir del programa.




