# Repte 4: Desenvolupeu un programa on s’hi introdueixi el nom d’un fitxer, amb el seu
# directori complet corresponent. Si el fitxer no existeix, el programa avisa a l’usuari que el
# fitxer no existeix o no es troba al directori introduït. Si el fitxer existeix, el programa copia
# tot el contingut d’aquest en un nou fitxer anomenat com l’original però situat en un
# directori un nivell més aprop al directory root. Si el fitxer original ja es troba al directori
# root, el programa no fa res.
# Exemple: Si introduïm /home/Desktop/file.txt , el programa ens genera un document
# anomenat file.txt al directori /home , que és una copia exacta del fitxer original. D’aquesta
# manera ara el document file.txt es troba a la direcció completa /home/Desktop/file.txt i a
# la direcció completa /home/file.txt .



