# Repte 2: Desenvolupeu un programa que creï un fitxer de text, que es guardi amb el nom
# corresponent al que hi posem com a primer argument i que com a contingut tingui el que
# hi posem al segon argument.

from sys import argv

file = open(argv[1], 'w')
file.write(argv[2])
file.close()