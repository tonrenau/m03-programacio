
# Programa que llegeix el contingut d'un fitxer de text i el mostra per pantalla
from sys import argv as argument

# Posar el contingut del fitxer dins d'una variable
try:
    file = open(argument[1])
    print(file.read())

except IndexError:
    print("El numero d'arguments introduits no es correcte (Programa Arg1)")

except FileNotFoundError:
    print("El fitxer especificat no s'ha trobat")

except UnicodeDecodeError:
    print("No es permet llegir fitxers amb aquest format")

finally:
    print("Gracies per utilitzar el programa")
    # tanca el fitxer abans de abortar
    file.close()

# Per executar el programa els dos fitxers s'han de trobar al mateix directori i
# nosaltres hem d'estar situat dins del mateix --> cd /Documents/Programacio/
