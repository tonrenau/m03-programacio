# prog amb argumanets 

import sys  # per no cridar la funcio tota l'estona --> from sys import argv "as nom"; 
# 			  llavorens per cridar la funcio nomes caldra fer: argv[n] "nom[n]".

def textlist(text_list):
    
    contatge = []

    for i in text_list:
        contatge.append(len(i))

    contatge.sort()

    return(contatge)


def create_hist(llista, simbol):
    num = 1
    while num <= llista[-1]:
        print(num, ":", symb * llista.count(num))
        num += 1


#Codi principal

text = sys.argv[1]
symb = sys.argv[2]
create_hist(textlist(text), symb)



# prog interactiu 
"""
#Definició funcions
def textlist(text):
    
    text_list = text.split()
    contatge = []

    for i in text_list:
        contatge.append(len(i))

    contatge.sort()

    return(contatge)


def creatte_hist(llista, simbol):
    num = 1
    while num <= llista[-1]:
        print(num, ":", symb * llista.count(num))
        num += 1


#Codi principal
text = input("introdueix una frase: ")
symb = input("quin symbol vols utilitzar? ")

creatte_hist(textlist(text), symb)

"""
