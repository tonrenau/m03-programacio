#Examen de funcions - part 2
#
# Dan Gómez Bernal
# 2022-2023
#
# Descripció: 
# Programa que demana a un usuari el seu nom,
# la seva edat i li pregunta si té dni.
# Si té dni retorna una llista amb totes les dades de l'usuari,
# en cas contrari, només mostra una llista amb el seu nom i l'edat.
#
# Especificacions d'entrada:
# 2 string (nom, te_dni), 1 int(edat)
#
# Joc de proves:
#               ENTRADA            SORTIDA
#
# joc1          Dan,20,SI          [Dan,20,"dni qualsevol"]
# joc2          Dan,20,NO          [Dan,20]
# joc3          Dan,20,si          ERROR: Has d'introduïr una resposta vàlida
#                                  SI/NO
#
# Pregunta: Es podria fer una altra funció per reduir encara més el
# codi principal?
# Crec que no es pot reduir més el codi principal, només demana al usuari
# el seu nom, edat, li pregunta si té dni o no i després crida una funció.
# El codi principal només són tres inputs i una crida a una funció, no crec
# que faci falta cap funció per reduir el codi.
# Com a molt, es podria afegir una funció nova per a validar que el dni sigui correcte,
# pero aixó no sería per reduir codi.




nom = input("Introdueix el teu nom: ")
edat = input("Introdueix la teva edat: ")
te_dni = input("Tens DNI? Respon amb SI/NO: ")



def gestionar_dni(nom, edat, resposta):
    '''
    funció que segons la resposta de l'usuari
    a si té dni o no, retorna la funció mostrar_dades amb
    nom, edat i dni en cas afirmatiu, i en cas negatiu
    només amb nom i edat.
    '''
    if resposta == "SI":
        dni = input("Introdueix el teu dni: ") 
        return(mostrar_dades(nom, edat, dni))
    
    elif resposta == "NO":
        return(mostrar_dades(nom, edat))
    
    
def mostrar_dades(nom, edat, dni=None):
    '''
    funció que retorna una llista en funció dels arguments
    que ha rebut.
    '''
    if dni == None:
        llista=[nom,edat]
    else:
        llista=[nom,edat,dni]
    return(llista)

def sortida(llista_dades):
    '''
    funció que mostra les dades ,en format de llista, 
    que l'usuari ha introduït i mostra error si la resposta
    no está en format correcte.
    '''
    if te_dni != "SI" and te_dni != "NO":
        print("ERROR: Has d'introduïr una resposta vàlida")
        print("SI/NO")
    print(llista_dades)
    return()
    

sortida(gestionar_dni(nom, edat, te_dni))