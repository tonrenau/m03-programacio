# python3
#
# EDT ASIX
# Ton Renau
# 2023-04-11
# 
# Descripcio: Programa on gestiona les dades de un usuari de nom, edat i DNI 
# 
# EE: nom, edat, te_dni --> input 
# 
# PREGUNTA_FINAL: Si, fent que la pregunta tens DNI?, estigues en una funcio si dius que "SI" demenar el numero, si respon que "NO" seguir amb el programa.
#
# FUNCIONS 

def mostrar_dades(nom,edat,te_dni):
"""
funcio on crea la llista de les dades de l'usuari, a partir de una llista buida i la funcio append
"""
	llista_user = []
	
	te_dni= False 
	if te_dni == "SI":
		te_dni = True 
		dni_user = input("Introdueix el teu DNI: ")
		
	if te_dni:
		llista_user.append(nom) 
		llista_user.append(edat)
		llista_user.append(dni_user)
	else:
		llista_user.append(nom)
		llista_user.append(edat)
	
	return llista_user
	
def gestionar_dni(nom,edat,dni):
"""
funcio on gestiona la llista i la aplica a una variable 
"""
	resultat=mostrar_dades(nom,edat,dni)
	print(resultat) 
	
def sortida(te_dni):
"""
funcio que determina si l'usuari a fet una bona entrada de la variable te_dni, sino retorna un misatge d'error.
"""
	if te_dni == "SI" or te_dni == "NO":
		mostrar_dades()
	else: 
		print ("Dada no valida")
		
# PROGRAMA PRINCIPAL

nom = input("Introdueix el teu nom: ")
edat = input("Introdueix la teva edat: ")
te_dni = input("Tens DNI? Respon amb SI/NO: ")

sortida(gestionar_dni(nom, edat, te_dni))
