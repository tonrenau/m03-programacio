import datetime 

def get_current_dt():
	data=datetime.datetime.now()
	return data 


def equal(date,day,month):
	date_split=date.split("/")
	valid = False 
	if day == int(date_split[0]) and month == int(date_split[1]):
		valid = True 
	return valid 
	
	
def increase_date(day,month,year):
	if last_day_month(day,month,year):
		if last_month_year(month):
			day=1
			month=1
			year += 1
		else:
			day = 1
			month +=1
	else:
		day +=1
	date= [day,month,year]
	return date
def last_day_month(day,month,year):
	if month in [1,3,5,7,8,10,12] and day ==31:
		return True
	elif month in [4,6,9,11] and day == 30:
		return True 
	else:
		if transfer_year(year):
			if month == 2 and day ==29:
				return True
		else:
			if month ==2 and day ==28:
				return True
	return False  
def transfer_year(year):
	return (year % 4 == 0) and ((year % 100 != 0) or (year % 400 == 0))

def last_month_year(month):
		if month == 12:
			return True
		return False
	
if __name__ == "__main__":
	print (get_current_dt())
	print (equal("20/03", 20 , 3))
	print ("\n Last day")
	print (last_day_month(20, 3, 2015))
	print (last_day_month(31, 1, 2015))
	print (last_day_month(28, 2, 2015))
	print ("\n Transfer Year")
	print (transfer_year(2020))
	print (transfer_year(2022))
	print ("\n Last Month")
	print (last_month_year(11))
	print (last_month_year(12))
	print ("\n increase Date")
	print (increase_date( 20,2,2022))
	print (increase_date( 31,1,2015))
	print (increase_date( 31,12,2022))
