import sys

dadesAlumnes = []

for i in range(int(sys.argv[1])):
    i += 1
    alumne = []
    print(f"Dades alumne {i}")
    dni = input("Introdueix un DNI: ")
    nom = input("Introdueix un nom: ")
    cognom = input("Introdueix un cognom: ")
    print("\n")

    alumne.append(dni)
    alumne.append(nom)
    alumne.append(cognom)

    dadesAlumnes.append(alumne)
    
print(dadesAlumnes)
