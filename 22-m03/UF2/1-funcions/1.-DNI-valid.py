# DNI valid 

def validacio(DNI): 
	
	def longitud(DNI):
		if len(DNI) ==9:
			return False

	def lletres(DNI):
		if DNI[:-1].isdigit():
			return False

	def primer_digit(DNI):
		if DNI[0] == "0":
			return False

	def comprovar_lletra(DNI):
		lletres = "TRWAGMYFPDXBNJZSQVHLCKE"
		pos_lletra = int(DNI[:-1]) % len(lletres)
		lletra_teorica = lletres[pos_lletra]
		resultat =  DNI[-1] == lletra_teorica 
		return resultat
	
	if not(comprovar_lletra (DNI)):
		return False 
	
	return True
	
		


"""
def valid(DNI):
	valid = False
	if len(DNI) ==9 :
		if DNI[:-1].isdigit():
			if DNI[0] != "0":
				lletres = "TRWAGMYFPDXBNJZSQVHLCKE"
				pos_lletra = int(DNI[:-1]) % len(lletres)
				lletra_teorica = lletres[pos_lletra]
				
				if DNI[-1] == lletra_teorica:
					valid = True
	return valid
"""
	
DNI = input("introdueix el DNI: ")

if validacio(DNI):
	print ("El DNI es valid")
else: 
	print ("El DNI no es valid")
