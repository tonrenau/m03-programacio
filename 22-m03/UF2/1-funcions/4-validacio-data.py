print ("Valorar la data")
"""
Fes una funció que validi si una cadena és una data correcta o no.
Per a nosaltres una data correcta és una data amb el format:
dd/mm/aaaa
on dd és un enter amb dos dígits (amb un zero davant si és menor a 10), 
mm un altre enter amb 2 dígits i 
aaaa un enter amb 4 dígits
A més de tenir el format correcte, els tres enters han de representar un dia real
(considerem que els anys han de ser positius).
"""


# def seperadors(data): esta mal 
	#if not data[2] == "/" and data[5] == "/":
	# 	return False
	
def seperadors(data):
	cont = 0 
	for i in data:
		if i == "/":
			cont += 1
	if cont == 2 :
		return True
	return False
	
def dd(data):
	dia=data[0:2]
	if dia.isdigit()==True:
		if int(dia) <= 0 or int(dia) >= 31:
			return False
	
def mm(data):
	mes=data[3:5]
	if mes.isdigit()==True:
		if int(mes) <=0 or int(mes]) >= 12:
			return False
def aaaa(data):
	any=data[6:]
	if any.isdigit() == True: 
		if int(any) < 0 or int(any) >= 9999:
			return False 
	



def valid(data):
	
	if seperadors(data) == False:
		return False
	if len(data) != 10:
		return False
	if dd(data) == False:
		return False 
	if mm(data) == False:
		return False 
	if aaaa(data) == False:
		return False 
	return True
	
		 
		
	
data_user = input("Introdueix una data valida en format dd/mm/aaaa: \n")
if valid(data_user):
	print("la data es valida")
else:
	print ("La data no es correcte")


