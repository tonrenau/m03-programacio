# Python program for implementation of Insertion Sort
 
# Function to do insertion sort
def insertionSort(llista):
 
    # Traverse through 1 to len(arr)
    for i in range(1, len(llista)):
 
        key = llista[i]
 
        # Move elements of arr[0..i-1], that are
        # greater than key, to one position ahead
        # of their current position
        j = i-1
        while j >= 0 and key < llista[j] :
                llista[j + 1] = llista[j]
                j -= 1
        llista[j + 1] = key
 
 
# Driver code to test above
llista = []
num_elem_llista = int(input("Introdueix cuants element vols ordenar:"))
for x in range(num_elem_llista):
    
    valor = int(input("Introdueix un valor enter: "))
    llista += [valor]
    
insertionSort(llista)
for i in range(len(llista)):
    print (llista[i])
