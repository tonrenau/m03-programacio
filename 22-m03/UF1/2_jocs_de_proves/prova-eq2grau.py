# programa eq 2 grau
# joc de proves 
# 					Entrada 			Sortida	
# Execucio 1 	 		eq1 (2sol): 2x**2 + 2x + 0 = 0 		x1: -1
#				(disc. = psoitiu)			x2: 0

# Execucio 2 			eq2 (1sol): x**2 + 2x + 1 = 0		x1 = x2 = -1
#				(disc. = 0)

# Execucio 3 			eq3 (0sol): 3x**2 + x + 1 = 0   	-
#				(disc. = negatiu)

# a <> 0 

a = float(input("a="))
b = float(input("b="))
c = float(input("c="))

disc = b**2 - 4*a*c 

x1 = (-b + (disc)**(1/2)) / (2*a)
x2 = (-b - (disc)**(1/2)) / (2*a)

print("x1=",x1) 
print("x2=",x2)
