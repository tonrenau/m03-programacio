"""
13. Conversió de base b a decimal
Fes un programa que faci conversions de base b a base 10.
El programa llegirà un primer nombre que serà la base i 3 dígits, un a un, corresponents al
nombre expressat en la base. Retornarà el nombre en base 10.
Exemple:
base = 3
dígits = 0, 1, 2 (012)
sortida: 5
"""

base = input("base del numero: ")
digit1 = input("Digit 1 de la base seleccionada: ")
digit2 = input("Digit 2 de la base seleccionada: ")
digit3 = input("Digit 3:de la base seleccionada ")

num = digit1 + digit2 + digit3
int_num = int(num)
int_base = int(base)

if digit1 >= base or digit2 >= base or digit3 >= base:  
    print ("Digit no valid per la base")
    exit

else: 
    print("si")

    