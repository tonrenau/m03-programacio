"""
Nombres perfectes
Segons la Viquipèdia, un nombre perfecte és un enter que és igual a la suma dels
seus divisors positius, excepte ell mateix. Així, 6 és un nombre perfecte, perquè els
seus divisors propis són 1, 2 i 3, i 6 = 1 + 2 + 3.
Els següents nombres perfectes són 28 i 496. Només se'n coneix un quart, i no se
sap si n'hi ha més o no.
Part teòrica
Comprova a ma que el nombre 28 és un nombre perfecte.

28 --> 1,2,4,7,14,28 
suma : 1+2+4+7+14 = 28 
"""

"""
Programa 1
Fes un programa que llegeixi un nombre i digui si és o no perfecte
"""

a = int(input(" Introdueix el num: "))

contador = 0 

for i in range (1,a):
		if a % i == 0:
			contador += i
if contador == a:
	print("Num. perfecte") 
if contador != a:
	print("Num. NO perfecte") 			
			
"""
Programa 2
Fes un programa que llegeixi un nombre enter n >= 0 i mostri els n primers nombres perfectes.
Si vols mostrar els 4 primers perfectes trigarà un pel, però si poses un nombre major a 4 pren-t’ho amb calma ja que no acabarà mai.
"""

trobats = int(input("Introdueix un nombre natural: "))
a = 0
n = 6

while a < trobats:

  sum = 0

  for i in range(1, n):

    if n % i == 0:

      sum += i

  if sum == n:

    print(n)
    a += 1

  n += 1
	
	













