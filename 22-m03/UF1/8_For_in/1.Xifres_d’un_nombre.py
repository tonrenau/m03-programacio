"""
Xifres d’un nombre
Quantes xifres té un nombre
Fes un programa que llegeixi un nombre enter i ens mostri les xifres que té.
Has de tenir en compte que fins ara només sabem utilitzar dades numèriques.
Justifica perquè el bucle és while o for.
Suma de les xifres d’un nombre
Fes un programa que llegeixi un nombre enter positiu i escrigui la suma de les seves
xifres.
"""

num = int(input("intodueix el numero: "))

xifres = 1

for i in range(1, num+1):
	if i == 10**xifres:
		xifres +=1

print (xifres)
