""" 
Comptar paraules
Compta el número de paraules que hi ha en una cadena de text llegida (les paraules
sempre estan separades per un o més espais en blanc).
Feu 2 versions: la primera considerant que la frase està ben formada i la segona en
que no sigui necessari (coses del tipus ‘ hola adeu ‘. Atenció amb el joc de proves).
"""
frase = input("intro. un text : \n")
paraules = 0 
comprobador = True 
i = 0 

while i < len(frase): 
	if frase[i] != ' ' and comprobador:
		paraules += 1
		comprobador = False 
		
	elif frase[i] == ' ' :
		comprobador = True
		
	i += 1
	
print("Hi ha", paraules,"paraules")
