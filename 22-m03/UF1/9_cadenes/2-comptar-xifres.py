"""
Comptar les xifres
Fes un programa que compti les xifres d’un nombre enter. A diferència del que hem
fet nosaltres, a les especificacions d’entrada no ens diuen que els nombres llegits
han d’estar ben formats, és a dir, que el joc de proves ha de tenir una entrada del
tipus 00003.
Utilitza dos mètodes per a resoldre aquest programa
● Comptant els zeros de l’esquerra
● Passant primer el nombre a enter i després a str
"""
# primera manera 

x = input() 
if x[0] == "+" or x[0]=="-":
	x = x[1:]
while x[0] == "0":
	x = x[1:]

print(len(x))

# segona manera 
x = input("Introdueix un numero: ")

a = int(x)
b = str(a)

print(len(b))
