"""
Generar subcadenes
Fes un programa en el que llegeixis una paraula i dos nombres menors que la
longitud d’aquesta paraula, el primer menor que el segon. El programa ha de
retornar la subcadena que comença en la posició del primer nombre i acaba abans
de la posició del segon.

Per exemple:
Entrada Sortida
El Python és molt fàcil 3 9 Python
Es tracta que facis un programa que faci el mateix que l’operador de slicing o de tall
[:]. És a dir que no el pots utilitzar.
Aclariment: Si utilitzem l’intèrpret interactiu:
>>> a = 'El Python és molt fàcil'
>>> a[3:9]
'Python'
"""

paraula = input("nom: ")

a = int(input("1re numero: "))
b = int(input("2n numero: "))

solucio = "\n"

for i in range (a,b):
	solucio += paraula[i]
	
print(solucio) 
	
