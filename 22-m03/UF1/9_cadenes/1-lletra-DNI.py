"""
Lletra del DNI
Resoldre la lletra del DNI:
La lletra del dni es pot calcular a partir dels seus nombres, i s’utilitza com a codi se
seguretat que ens permet adonar-nos quan l’hem dit malament.
Per a calcular-la, només cal que divideixis el nombre per 23 i quedar-te amb el
residu. Aquest residu és un nombre entre 0 i 22, i la lletra del dni correspon a la
lletra que ocupa la posició de la cadena següent:
TRWAGMYFPDXBNJZSQVHLCKE
D’aquesta manera, si el residu dona 0, la lletra és la T, si el residu és 1 la lletra és la
R i així successivament
Fes un programa que llegeixi el nombre del DNI i mostri la lletra que li correspon
"""
