"""
Volem fer un programa que ens digui si un nombre és múltiple de 2, de 3 i de 10,
però volem fer-lo tal i com ens van ensenyar a l’escola, és a dir, amb els següents
algorismes:
● Un nombre és múltiple de 2 si acaba en 0, 2, 4, 6, o 8
● Un nombre és múltiple de tres si sumant les seves xifres ens dóna un múltiple
de tres. És a dir, si anem sumant les seves xifres fins que ens quedi una
única xifra, aquesta xifra és 3, 6 o 9
● Un nombre és múltiple de 10 si acaba en 0
El resultat de l’execució per al nombre 256296 seria el següent:
256296 es múltiple de dos:
True
256296 es múltiple de tres:
True
256296 es múltiple de deu:
False
Fes aquest programa suposant que les especificacions d'entrada són per a un
nombre enter.
Per a que surti aquest programa, cal que tingueu molt clar el tipus de variable que
utilitzeu en cada moment (int o str), i que feu els canvis entre els tipus utilitzant les
funcions int() i str()
"""

num = input("intro. un numero: \n")
 
mult_dos = False 
mult_tres = False
mult_deu = False 

if num[-1] == "0" or num[-1] == "2" or num[-1] == "4" or num[-1] == "6" or num[-1] == "8":
	mult_dos = True
	

n = num
while 1 <= len(n):
	suma = 0
	for i in n:
		suma += int(i) 
	n = str(suma) 
mult_tres = n == "3" or n == "6" or n == "9":
	

	

if num[-1] == "0":
	mult_deu = True
	
print (num, "es multiple de dos", mult_dos, \
"\n", num, "es multiple de tres", mult_tres, \ 
"\n", num, "es multiple de deu", mult_deu)




