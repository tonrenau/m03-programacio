"""
Part pràctica		
Cal entregar els programes al moodle, en una carpeta on consti el vostre nom
Atenció:
    • Fes un fitxer per cada exercici
    • Un exercici sense capçalera serà un zero
    • Un programa sense les especificacions d’entrada serà un 0
    • Un programa que no s’executa serà un 0.
    • Tens un refinament que has de seguir per al programa 4
"""

"""
Programa 1 (dir si un nombre és perfecte): 			(2 punts)
Un nombre perfecte és un enter que és igual a la suma dels seus divisors positius, excepte ell mateix. 
Així, el 6 és un nombre perfecte perquè els seus divisors propis són 1, 2 i 3, i 
6 = 1 + 2 + 3. 
Fes un programa que llegeixi un nombre i digui si és o no perfecte. 
Desa el joc de proves a la capçalera d’aquest programa.
"""

print ("Nombre perfecte")

# Ton Renau Mirambell
# ex 1

# 1 especificacio d'entrada "int", ja que vurem si es un nombre perfete.
# Joc de proves 
# Entrada 		Sortida 
#   6			Si, el 6 es un nombre perfecte
#	8 			No, el 8 no es un nombre perfecte 

n = int(input("Escriu un nombre enter positiu: ")) 

if n <= 0:
	 print ("El nombre a de ser enter positiu!!") 

else:
	i = 1
	suma = 0 
	while i < n:
		if n % i == 0:
			suma += i
		i += 1
	
	if n == suma:
		print ("Si, el", n,"es un nombre perfecte")
	else:
		print ("No,el", n,"no es un nombre perfecte")



"""
Programa 2 (buscar els n primers perfectes): 			(2 punts)
Si el programa anterior no funciona no corregiré aquest
Els següents nombres perfectes són el 28 i el 496. 
Només se'n coneix un quart, i no se sap si n'hi ha més o no.
Volem fer un programa que llegeixi un nombre n i ens mostri els n primers 
nombres perfectes (evidentment, si n és major que 4 el programa es quedarà 'pensant' 
la solució fins que no interrompem l'execució).
"""
print ("Buscador nombre perfecte")
trobats = int(input("Introdueix un nombre natural: "))
a = 0
n = 6

while a < trobats:

  sum = 0

  for i in range(1, n):

    if n % i == 0:

      sum += i

  if sum == n:

    print(n)
    a += 1

  n += 1
		


"""
Programa 3 (treure les potències de dos d’un nombre):	(2 punts)
Volem fer un programa que ens divideixi un nombre entre 2 tantes vegades com sigui possible, 
i ens mostri el que queda.
Per exemple:
Entrada: 		Sortida:
200				25
8				1

Desa el joc de proves i les especificacions d’entrada en la capçalera del fitxer on hi hagi aquest programa.
"""

print ("potencies de 2 d'un nombre")

n_antes = int(input("Escriu un nombre enter positiu: "))

con = 0

while n_antes > 2:
	n_new = n_antes / 2	
	
	 n_antes = n_new 
	 con += 1

print (con)

"""
Programa 4 (buscar els factors primers):				(2 punts)   
Si el programa anterior no funcions no corregiré aquest
Volem escriure un programa que ens mostri els divisors primers d’un nombre, 
però utilitzant l’algorisme que ens van ensenyar de petits.
ATENCIÓ: Tens un fitxer on hi ha el refinament d’aquest programa. L’has de seguir.
"""










