"""
Escriure tots els divisors d’un nombre en ordre ascendent
Per exemple, si el nombre llegit és el 12, el progama mostrarà
1, 2, 3, 4, 6, 12 (pot fer-ho amb un nombre per línia).
"""

n = int(input("num: ")) 
i = 0 
while i <= n :
	if n % i == 0:
		print (i) 
	i += 1
	
