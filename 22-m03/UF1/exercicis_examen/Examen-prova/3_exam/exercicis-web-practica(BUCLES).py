""" 
Ejercicio 1

Escribir un programa que pida al usuario una palabra y la muestre por pantalla 10 veces.

paraula = input() 
i=1
while i < 10 :
	print (paraula) 
	i += 1 
"""

"""
Ejercicio 2

Escribir un programa que pregunte al usuario su edad y muestre por pantalla todos los años que ha cumplido (desde 1 hasta su edad).

edat = int(input("Quants anys tens??")) 
i = 0
while i < edat :
	i += 1 
	print ("Has cumplit",i,"anys")
"""

"""
Ejercicio 3

Escribir un programa que pida al usuario un número entero positivo y 
muestre por pantalla todos los números impares desde 1 hasta ese número separados por comas.

num= int(input("numero enter positiu:" ))
for i in range(1, num+1, 2):
	print(i, end=", ")
"""

"""
Ejercicio 4

Escribir un programa que pida al usuario un número entero positivo y 
muestre por pantalla la cuenta atrás desde ese número hasta cero separados por comas.

n = int(input())
for i in range (n, -1, -1):
	print(i)
"""

"""
Ejercicio 5

Escribir un programa que pregunte al usuario una cantidad a invertir, el interés anual y 
el número de años, y muestre por pantalla el capital obtenido en la inversión cada año que dura la inversión.


"""

"""
Ejercicio 6

Escribir un programa que pida al usuario un número entero y muestre por pantalla un 
triángulo rectángulo como el de más abajo, de altura el número introducido.

*
**
***
****
*****

n = int(input("Introduce la altura del triángulo (entero positivo): "))
for i in range(n):
   print("*"*(i+1))
"""


"""
Ejercicio 7

Escribir un programa que muestre por pantalla la tabla de multiplicar del 1 al 10.

"""




"""
Ejercicio 8

Escribir un programa que pida al usuario un número entero y muestre por pantalla un triángulo rectángulo como el de más abajo.

1
3 1
5 3 1
7 5 3 1
9 7 5 3 1

n = int(input())
for i in range (n):
	 
""" 




""" 
Ejercicio 9

Escribir un programa que almacene la cadena de caracteres contraseña en una variable, 
pregunte al usuario por la contraseña hasta que introduzca la contraseña correcta.

key = "calbo" 
contra =""
while contra != key:
	contra = input("contrasenya: ")
	
print ("correct ;)")
"""



"""
Ejercicio 10

Escribir un programa que pida al usuario un número entero y muestre por pantalla si es un número primo o no.

n = int(input("Introduce un número entero positivo mayor que 2: "))
i = 2
while n % i != 0:
    i += 1
if i == n:
    print( n,"es primo")
else:
    print(n,"no es primo")
"""



"""
Ejercicio 11

Escribir un programa que pida al usuario una palabra y luego muestre por pantalla una a una las letras 
de la palabra introducida empezando por la última.

w = input("Paraula: ")
 
for i in range(len(w)-1,-1,-1):
	print (w[i])

"""

"""
Ejercicio 12

Escribir un programa en el que se pregunte al usuario por una frase y una letra, 
y muestre por pantalla el número de veces que aparece la letra en la frase.

frase = input("escriu una frase: ")
lletra = input("escriu una lletra: ")

cont = 0 
for i in frase:
	if i == lletra:
		cont += 1
print("La lletra",lletra,"apareix",cont,"vegades")
"""



