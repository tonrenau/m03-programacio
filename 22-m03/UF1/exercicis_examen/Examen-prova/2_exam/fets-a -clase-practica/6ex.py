"""
Exercici 6	(3 punts)
Fes el següent programa:
Llegeix dos nombres enters i digues si un és divisor de l'altre o no.
Tens un fitxer que es diu ‘divisors.py’ on hi ha el refinament (els passos) que has de seguir. 
També hi ha el joc de proves (el mateix que vam fer a classe, però en comptes de dir si o no, 
diem True o False)
ATENCIÓ: Si fas un programa utilitzant un algoritme que no és el que t’indica el refinament del fitxer, 
serà un 0 tot I que el programa funcioni correctament.
L’última línia del programa no es pot modificar. Si ho feu, és que esteu seguint un mètode diferent.
"""



# capçalera complerta

'''
joc de proves
Entrada 			sortida
10, 5				True
5, 10				True
3,  2				False
-10, -2				True
-3, 2				False
-10, 5				True

Especificacions d'Entrada: 2 nombres enters diferents de 0
'''

#ATENCIÓ: Aquest programa ha de seguir aquest refinament. En cas contrari,
#serà un zero ni que funcioni correctament

#llegim dos enters
n1 = int(input())
n2 = int(input())

#els modifiquem (si cal) per tenir els valors absoluts del dos nombres 
n1 = (n1**2)**(1/2)
n2 = (n2**2)**(1/2)

#intercanviem les variables (si cal) perquè el primer sigui major que el segon
if n1 > n2:
	n3 = n2	
	n2 = n1
	n1 = n3
	 
if n2 > n1:
	n3 = n1
	n1 = n2
	n2 = n3

#mostrem si el segon és divisor del primer o no
print(n1 % n2 == 0)

