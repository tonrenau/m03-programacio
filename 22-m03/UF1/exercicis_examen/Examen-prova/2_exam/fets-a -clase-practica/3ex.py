"""
Volem garantir que en un programa entrin només ips privades amb els següents formats: 
10.X.X.X, 172.16.X.X i 192.168.X.X. Les ips de xarxa(.0) i de broadcast (.255) les podeu considerar vàlides.
Les dades d’entrada del programa són ip1, ip2, ip3 I ip4, 4 enters qualsevol
Troba una expressió booleana que validi les ips entrades.
"""

# 4 especificacions de entrad ade cada ip 
# joc de proves
# 					entrada 			sortida
# 1: 				10.0.0.0 				true
# 2: 				172.16.0.0 				true
# 3:				172.10.0.2				false 
# 4: 				192.168.0.255			true 
ip1 = int(input("ip1:"))
ip2 = int(input("ip2:"))
ip3 = int(input("ip3:"))
ip4 = int(input("ip4:"))

ip = ip1 == 10 or (ip1 == 172 and ip2 == 16) or (ip1 == 192 and ip2 == 168) or ip4 == 0 or ip4 == 255

print (ip) 
