"""
Exercici 2		(0.5 punts)
Fes un joc de proves per al següent programa:
Llegir tres nombres enters i ordenar-los de menor a major.
"""

# 3 especificacions de entrada int
# Joc de proves 
# 				entrada			sortida
# 			a=4, b=2, c=3 		 2 < 3 < 4 
# 			a=1, b=1, c=2		 1, 1 < 2 
# 			a=3, b=3, c=3		 	3,3,3

# El meu programa: 

"""
a = int(input("a="))
b = int(input("b="))
c = int(input("c="))


if a < b and b < c:
	print (a,"<",b,"<",c)

elif a < c and c < b: 
	print (a,"<",c,"<",b)

elif b < c and c < a:
	print (b,"<",c,"<",a)

elif b < a and a < c:
	print (b,"<",a,"<",c)

elif c < a and a < b:
	print (c,"<",a,"<",b)

elif c < b and b < a:
	print (c,"<",b,"<",a)

elif a == b:
	
	if b==c:
		print (a,b,c)
	elif b < c:
		print (a,b,"<",c)
	elif b > c:
		print (c,"<",a,b)

elif b == c:
	
	if a < c:
		print (a,"<",b,c)
	elif a > c:
		print (b,c,"<",a)
	
"""


# estructura molt millorada amb nomes tres condicionals enves de 13 com hi ha en el anterior 

"""
num1 = int(input("num1:"))
num2 = int(input("num2:"))
num3 = int(input("num3:"))

if num1 > num2 :
	num4 = num1
	num1 = num2
	num2 = num4

if num2 > num3: 
	num4 = num2
	num2 = num3
	num3 = num4

if num1 > num2 :
	num4 = num1
	num1 = num2
	num2 = num4
	
print (num1, num2, num3) 

"""


a = int(input("Introdueix un nombre enter: "))
b = int(input("Introdueix un nombre enter: "))
c = int(input("Introdueix un nombre enter: "))

gran = a
mitja = b
petit = c

if b > a:
	gran = b
	mitja = a
	if c > a:
		mitja = c
		petit = a
		if c > b:
			gran = c
			mitja = b

elif c > a:
	gran = c
	mitja = a
	petit = b
	
elif c > b:
	mitja = c
	petit = b
	
print(petit, mitja, gran)





