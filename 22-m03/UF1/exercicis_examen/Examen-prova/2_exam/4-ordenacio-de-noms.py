"""
Exercici 4 (2 punts)
Realitza un programa que llegeixi quatre noms i els mostri per pantalla ordenats per ordre
alfabètic ascendent. Com a màxim pots utilitzar 14 expressions booleanes, és a dir, no pots
utilitzar més de 14 condicionals (if, elif, else).
"""

# 4 especificacions de entrada que son 4 noms:
# Joc de proves 
# 	Entrada					sortida
#	nom1: Ton
#	nom2: Elda
#	nom3: Joan				Elda, Joan, Laia, Ton
#	nom4: Laia 				
nom1 = str(input("introdueix el 1r nom:"))
nom2 = str(input("introdueix el 2n nom:"))
nom3 = str(input("introdueix el 3r nom:"))
nom4 = str(input("introdueix el 4t nom:"))

#intercanviem les variables (si cal) perquè el primer sigui el major sempre
if nom1 > nom2 :
	nom5 = nom2
	nom2 = nom1
	nom1 = nom5 

if nom1 > nom3 :
	nom5 = nom3
	nom3 = nom1
	nom1 = nom5

if nom1 > nom4 :
	nom5 = nom4
	nom4 = nom1
	nom1 = nom5
	
if nom2 > nom3 :
	nom5 = nom3
	nom3 = nom2
	nom2 = nom5
	
if nom2 > nom4:
	nom5 = nom4
	nom4 = nom2
	nom2 = nom5 

if nom3 > nom4:
	nom5 = nom4
	nom4 = nom3
	nom3 = nom5
	
print( nom1,nom2,nom3,nom4) 
