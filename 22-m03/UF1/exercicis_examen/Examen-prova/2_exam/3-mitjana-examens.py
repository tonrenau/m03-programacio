"""
Exercici 3 (2,5 punts)
Realitza un programa que llegeixi els valors de les notes de dos exàmens i en mostri per pantalla
la nota mitjana. 
Ara bé, si algun examen no s’ha realitzat, no s’introduirà cap nota per aquell
examen sinó que s’introduiran les sigles NP (de no presentat). 
Si un dels dos exàmens no s’ha realitzat i l’altre sí (i per tan aquest sí que té nota), 
la nota mitjana que es mostra per pantalla  és la mateixa nota de l’examen que s’ha realitzat. 
Si cap dels exàmens s’ha realitzat, es mostra per pantalla el següent missatge: 
“No s’ha realitzat cap dels dos exàmens”.
"""

# 2 especificacions de entrada, que donen referencia a les dos notes dels examens 
# joc de proves 
# 		Entrada 					sortida
# 	examen1 = 7
#	examen2 = 5 					nota final de l'avaluacio = 6
#
# 	examen1 = 7
#	examen2 = NP 					nota final de l'avaluacio = 7
#
# 	examen1 = NP
#	examen2 = NP					No s'ha realitzat cap dels dos esxamnes 


exam1 = float(input("introdueix la nota del 1r examen, si algun examen no s'ha fet introdueix un 11: "))
exam2 = float(input("introdueix la nota del 2n examen, si algun examen no s'ha fet introdueix un 11: "))

NP = "11" 

if (exam1 >= 0 and exam1 <= 10) and (exam2 >= 0 and exam2 <= 10):
	x = ((exam1 + exam2) / 2)
	print ("nota final de l'avaluacio = ",x)
elif (exam1 >= 0 and exam1 <= 10) and exam2 == 11:
	x = exam1
	print ("nota final de l'avaluacio = ",x)
elif (exam2 >= 0 and exam2 <= 10) and exam1 == 11:
	x = exam2 
	print ("nota final de l'avaluacio = ",x)

else: 
	print("No s'ha realitzat cap dels dos examens")
