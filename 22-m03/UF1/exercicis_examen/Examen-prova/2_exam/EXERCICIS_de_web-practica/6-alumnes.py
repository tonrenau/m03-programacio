"""
Ejercicio 6

Los alumnos de un curso se han dividido en dos grupos A y B de acuerdo al sexo y el nombre.
El grupo A esta formado por las mujeres con un nombre anterior a la M y los hombres
con un nombre posterior a la N y el grupo B por el resto.
Escribir un programa que pregunte al usuario su nombre y sexo, 
y muestre por pantalla el grupo que le corresponde.
"""

# 2 especificacions d'entrada amb dos str 
# joc de proves 
#			entrada				sortida
# 			nom: Elda
#			sexe: F				A
#
#			nom: Ton
#			sexe: M				A
#
#			nom: Sara 
#			sexe: F				B
#
#			nom: Jordi
#			sexe: M 			B

nom = str(input("nom: "))
sexe = str(input("sexe (M o F): ")) 

if sexe == "F":
	
	if nom [0] >= "A" and nom [0] <= "M":
		group = "A"
		
	else:
		group = "B"
		

elif sexe == "M" :
	
	if nom [0] >= "N" and nom [0] <= "Z":
		group = "A" 
		
	else: 
		group = "B"

print("Group:", group)

		
