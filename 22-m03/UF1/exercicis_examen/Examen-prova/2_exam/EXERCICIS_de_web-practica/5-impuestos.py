"""
Ejercicio 5

Para tributar un determinado impuesto se debe ser mayor de 16 años 
y tener unos ingresos iguales o superiores a 1000 € mensuales. 
Escribir un programa que pregunte al usuario su edad y sus ingresos 
mensuales y muestre por pantalla si el usuario tiene que tributar o no.
"""

# 2 entrades una int donant referencia ala edat i a l'altre un float donant referencia al sou de la persona
# Joc de proves 
# 			entrada 		 			sortida 
# 			edat=16, sou=1000			valid
#			edat=15, sou=1000			No valid
#			edat=20, sou=400 			No valid 

edat = int(input("edat: "))
sou = float(input("sou: "))

if edat <= 16 or sou < 1000:
	print("No valid")

else: 
	print("valid")


