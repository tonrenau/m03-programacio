"""Ejercicio 3

Escribir un programa que pida al usuario dos números y muestre por pantalla su división. 
Si el divisor es cero el programa debe mostrar un error.
"""

# 2 entrades float 
# Joc de proves
# 			entrada 		sortida
#			a=2, b=2 			1 
#			a=2.5, b=1			2,5
# 			a=3.67, b=0 		error

a = float(input("a="))
b = float(input("b="))

if b==0 :
	print("error")

else:
	print( a / b )
