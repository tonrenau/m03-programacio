"""
Ejercicio 9

Escribir un programa para una empresa que tiene salas de juegos para todas las edades 
y quiere calcular de forma automática el precio que debe cobrar a sus clientes por entrar. 
El programa debe preguntar al usuario la edad del cliente y mostrar el precio de la entrada. 
Si el cliente es menor de 4 años puede entrar gratis, si tiene entre 4 y 18 años debe pagar 5€ 
y si es mayor de 18 años, 10€.
"""

# 1 especificacio d'entrada on fa referencia a la edat de la persona 

# joc de proves 
# 		entrada				sortida
#		2 anys 				free
#		15 anys 			5 euros
# 		20 anys 			10 euros

edat = int(input(" Introdueix la teva edat: "))
if edat < 4 :
	ticket = 0 
elif edat >= 4 and edat <= 18 :
	ticket = 5
else:
	ticket = 10 
	
print (ticket,"euros")
