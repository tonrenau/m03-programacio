"""
Ejercicio 4

Escribir un programa que pida al usuario un número entero y muestre por pantalla si es par o impar.
"""


# 1 entrada de un numero en enter 
# Joc de proves 
# 		entrada				Sortida
# 		2 					par 
#		1 					impar

num = int(input("num= "))

if num % 2 == 0:
	print(" numero par")

else: 
	print(" numero impar")
