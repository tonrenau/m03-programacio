"""
Exercici 2 (2,5 punts)
Realitza un programa que llegeixi un nombre enter positiu que pot tenir entre 1 i 5 xifres. El
programa mostra per pantalla el missatge “El nombre té x xifres”, on x és el nombre de xifres
que té el nombre. No es pot utilitzar el mètode len() per a realitzar aquest programa.
"""

# 1 especificacio d'entrada, que fa referencia al nombre de entre 1 i 5 xifres 
# joc de proves
# 			entrada 			sortida
# 		num = 123 				El nombre te 3 xifres  

num = int(input(" introdueix un nombre de entre 1 i 5 xifres: "))

if num < 9:
	x = 1
elif num >= 10 and num < 99:
	x = 2 
elif num >= 100 and num < 999:
	x = 3
elif num >= 1000 and num < 9999:
	x = 4
elif num >= 10000 and num < 99999:
	x = 5 
else :
	x = "mes xifres de les permeses, sisplau apren a llegir quantes" 
print("El nombre te", x, "xifres")
