"""
Examen de programació condicional
Els següents programes han de tenir una capçalera amb el format i la informació adequada, en
la que hi ha de figurar, entre altres coses, la descripció, les especificacions d’entrada i el joc de
proves del programa. Qualsevol ús de la programació que no sigui purament seqüencial o
condicional suposarà un 0 en l’exercici o exercicis corresponents. Només es poden utilitzar
conceptes i mètodes tractats a classe en les sessions de programació seqüencial i condicional.
"""

"""
Exercici 1 (3 punts)
Realitza un programa que llegeixi tres nombres naturals de tres xifres cadascun i un quart
nombre natural d’una sola xifra (que anomenarem “xifra de referència”). 
El programa ha de mostrar per pantalla aquell dels tres primers nombres que tingui més xifres iguals a la 
xifra de referència. 
Si dos nombres tenen el mateix número de xifres iguals a la xifra de referència, i
aquest dos nombres tenen més xifres iguals a la xifra de referència que no pas l’altre nombre
restant, el programa mostra per pantalla aquests dos nombres.
Si tots tres nombres tenen el mateix número de xifres iguals a la xifra de referència, 
el programa mostra per pantalla els tres nombres.
"""

# 4 especificacions de entrada on 3 seran 3 nombres de 3 xifres cada un i la quarta que sera la xifra especifica.

# Joc de Proves
# 			Entrada 					Sortida
#		xifra1 = 123
#		xifra2 = 345
# 		xifra3 = 789 		
# 		xifraR = 7						xifra3 = 789 
#
#		xifra1 = 133
#		xifra2 = 343
# 		xifra3 = 739 					xifra1 = 133 
#		xifraR = 3 						xifra2 = 343
# 
#		xifra1 = 666
#		xifra2 = 666
# 		xifra3 = 666	
#		xifraR = 6 						xifra1 = xifra2 = xifra3 = 666

xifra1 = str(input("introdueix el valor del numero 1 de 3 xifres: "))
xifra2 = str(input("introdueix el valor del numero 2 de 3 xifres: "))
xifra3 = str(input("introdueix el valor del numero 3 de 3 xifres: "))
xifraR = str(input("introdueix el valor del numero referent de 1 xifra: "))

if xifraR == (xifra1 [0] or xifra1 [1] or xifra1 [2]):
	numR1 = xifra1
elif xifraR == (xifra2 [0] or xifra2 [1] or xifra2 [2]):
	numR2 = xifra2
elif xifraR == (xifra3 [0] or xifra3 [1] or xifra3 [2]):
	numR3 = xifra3

print(numR1, numR2, numR3)
