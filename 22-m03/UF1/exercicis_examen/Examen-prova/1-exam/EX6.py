"""
Entre els 487 convidats hi ha tots els professors del departament (que som 17). Com que
tenim molta feina endarrerida, el cap de departament vol que aprofitem el moment per a
fer una reunió. Això vol dir que ens hem de posar tots en la mateixa taula.
* Com que el més important per a mi és que totes les taules tinguin el mateix nombre de convidats, ara vull fer una distribució en taules de 17 persones. Com quedarà aquesta distribució?
* Una altra possible distribució seria afegir el nuvi a la nostra taula i fer-les de 18 persones (tot i que el nuvi no és del departament, sempre troba molt interessants tots els temes que tractem). 

Què m'aconselles i perquè? 
Recorda que només hi pot haver una única taula, com a màxim, amb un nombre diferent de comensals.
Respon a la capçalera del fitxer
"""

#joc de proves
# 			Entrada			Sortida	
# EX1:			17 persones 		1 taula, 0 sobrants
# EX2: 			10 persones 		0 taules, 10 sobrants
# EX3: 			20 persones 		1 taula, 3 sobrants

num_convidats = int(input("Numero de convidats:"))

taules = num_convidats // 17
sobrants = num_convidats % 17
 
print("taules necessaries:", taules)
print("persones a la taula sobrant:", sobrants)

# En aquest cas hi hauran 28 taules totals i 11 persones a la taula sobrant

