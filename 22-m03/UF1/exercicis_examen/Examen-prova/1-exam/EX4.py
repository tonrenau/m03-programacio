"""
Volem fer un programa que simuli un rellotge. Cal que llegeixi l'hora, minut i segon per separat, i ens mostri el segon següent.
Per exemple: llegim 13, 3, 12 i em mostra 13, 3, 13
Fes un joc de proves i les especificacions d’entrada per a aquest programa.
"""

# programa rellotge 

# 3 especificacions de entrades

# joc de proves
# 				 	Entrada 		Sortida 
#	Execucio 1:		        1 				1
#					2				2	
# 					3				4	
#	execucio 2:                     23				0	
#					59				0	
#					59 				0

hores_i = int(input("hores:"))
min_i = int(input("min:"))
seg_i = int(input("seg:"))

seg_i = seg_i + 1
seg = seg_i % 60 
min_i = min_i + seg_i // 60 
min_f = min_i % 60
hores = hores_i + min_i // 60 
hores = hores % 24 

print( hores, min_f , seg)

