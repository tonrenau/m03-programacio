"""
Em vull casar la setmana vinent i tinc un problema amb les taules i els convidats. He pensat en convidar 431 persones i m'agradaria fer taules de 12 persones. 
Com que els informàtics som molt metòdics i frikis, 
vull que a cada taula hi hagi exactament 12 convidats excepte possiblement en una taula, on hi posaré els convidats que 'sobrin'.
Vull fer un programa que em llegeixi el nombre de convidats, em digui les taules que necessitaré i de quantes persones serà la 'taula diferent'.
Fes un joc de proves i les especificacions d'entrada per a aquest programa.
Vull fer un programa que em llegeixi el nombre de convidats i em digui les taules que
necessitaré i de quantes persones serà la 'taula diferent'.
""" 

#joc de proves 
#431 persones en taules de 12 persones 
#       entrada    		sortida 
# EX1: 	9 persones 		0 taules, 9 sobrants
# EX2:	25 persones 		2 taules, 1 sobrant
# EX3:  12 persones 		1 taules,  0 sobrants 

num_convidats = int(input("Numero de convidats:"))

taules = num_convidats // 12
sobrants = num_convidats % 12
 
print("taules necessaries:", taules)
print("persones a la taula sobrant:", sobrants)

# en 431 persones tindrem 35 taules i 11 persones sobrants a la ultima taula.
 
