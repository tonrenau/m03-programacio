"""
Programa 3 (5,5 punts).
Un nombre en una matriu és un "punt de sella" si és el
mínim nombre de la seva fila i el màxim nombre de la seva columna. Fes un
programa que donada una matriu m de nombres enters compresos entre el -9999 i
el 9999, i dos nombres enters no negatius r i c, determini si el nombre situat a la fila
r i columna c és un "punt de sella".
Per exemple, donada la següent matriu:

1 2 3
4 5 9
2 8 6
1 0 7

- Si r = 1 i c = 0, m[1][0] és 4 i és un punt de sella
- Si r = 2 i c = 2, m[2][2] és 6 i NO és un punt de sella
- Si r = 0 i c = 0, m[0][0] és 1 i NO és un punt de sella
- Si r = 1 i c = 1, m[1][2] és 9 i NO és un punt de sella

L’usuari ha d’indicar inicialment, la mida de la matriu que vol introduir, indicant en
primer lloc quantes files vol que tingui la matriu i en segon lloc el número de
columnes d’aquesta. Seguidament, el programa li demana a l’usuari, nombre per
nombre, que vagi introduïnt els elements de la matriu. Un cop creada la matriu, i just
abans de demanar-li a l’usuari els nombres r i c per saber si un nombre en concret
és un punt de sella de la matriu, s’ha de mostrar per pantalla la matriu, de manera
neta i estructurada (utilitza els f-strings).
El programa ha de presentar per pantalla un resultat final d’execució amb la següent
forma i estructura (tingues en compte que aquest és un exemple, per mostrar la
manera en que es vol que el programa interactui amb l’usuari):
"""
#
# Ton Renau Mirambell
# Curs 2022-23
# Escola del treball 
#
# examen final UF1 programacio 
# exercici 2
# 
# començem definint les dimensions de la matriu, i ficant els numeros a les seves pocicions r, c 
fila = int(input("Introdueix el nombre de files de la matriu : "))
columnes = int(input("Introdueix el nombre de columnes de la matriu : "))
matriu = []
for r in range (fila):
	fila=[]
	for c in range (columnes):
		element = int(input(f"introdueix l'element {r} {c} de la matriu: "))
		fila.append(element)
	matriu.append(fila)

for r in matriu :
	mat = ""
	for c in r:
		 mat += f"{c}       "
	# representacio de la matriu creada 
	print (mat,"\n")

print("Analisis d'element per determinar si es un punt de sella: \n")

r = int(input("intodueix la posicio en la columna: "))
c = int(input("intodueix la posicio en la fila: "))

num=matriu[r][c]

for x in range (r,[0,columnes]):
	if 
for y in range ([0,files],c):








