"""
Programa 1 (4,5 punts).
 Realitza un programa que simuli el Sign Up (la creació
d’un usuari) d’una pàgina web. Les característiques i funcionalitats del Sign Up són
les següents:

El programa li demanarà a l’usuari que introdueixi el seu nom (o identificador)
d’usuari i seguidament li demanarà que el torni a introduir.

1. Si els dos noms no coincideixen, es mostrarà per pantalla un missatge d’error
corresponent (crea’l tu) i s’acabarà l’execució del programa.

2. Si les introduccions dels noms coincideixen, però no l’usuari no ha escrit un
identificador vàlid, es mostrarà per pantalla un missatge d’error corresponent
(crea’l tu) i s’acabarà l’execució del programa. Un nom serà un identificador
vàlid si solament conté lletres majúscules (A-Z) i/o minúscules (a-z), dígits
(0-9) i/o guions baixos (_). Un identificador vàlid no pot començar amb un
número ni contenir cap espai. Utilitza el mètode per a cadenes isidentifier()
per a realitzar aquest filtratge.

3. Si les introduccions dels noms coincideixen, l’usuari ha escrit un identificador
vàlid, però aquest coincideix amb el d’un usuari que ja està inscrit, es
mostrarà per pantalla un missatge d’error corresponent (crea’l tu) i s’acabarà
l’execució del programa. Just al principi del programa crea una llista amb 5
identificadors d’usuari diferents que ja estiguin inscrits (inventa’t els
identinficadors). Utilitzarem aquesta llista, de manera fictícia, com si s’hagués
creat a partir de la base de dades on s’hi troba la informació dels usuaris
inscrits a la web. Utilitza el mètode per a llistes count() per comprovar si
l’identificador que ha introduït l’usuari es troba a la llista.

4. Si les introduccions dels noms coincideixen, l’usuari ha escrit un identificador
vàlid i aquest no coincideix amb el d’un usuari que ja està inscrit, es mostrarà
per pantalla un missatge de benvinguda (crea’l tu). A més, s’afegirà
l’identificador de l’usuari nou a la llista d’identificadors existents, situant-lo a la
primera posició (la posició 0 de la llista), de manera que la llista d’usuaris
passarà a tenir la següent estructura: [id_usuari_nou, id_usuari_vell1,
id_usuari_vell2, id_usuari_vell3, id_usuari_vell4, id_usuari_vell5]. Utilitza el
mètode per a llistes insert() per a afegir l’usuari nou a la primera posició.

Finalment, si l’usuari s’ha inscrit correctament, el programa mostra per pantalla,
situats en la mateixa fila, els noms dels usuaris existents, en l’ordre invers a com
estan emmagatzemats a la llista d’usuaris. De manera que es mostraran per
pantalla 6 identificadors d’usuari de la següent manera:

id_usuari_vell5, id_usuari_vell4, id_usuari_vell3, id_usuari_vell2, id_usuari_vell1, id_usuari_nou


"""
# Ton Renau Mirambell
# Curs 2022-23
# Escola del treball 
#
# examen final UF1 programacio 
# exercici 1 
# 
# 2 especificacions de entrda que son dos noms per el log in
# 
#creacio de la llista de usuaris
llista_users = "Pol01", "Natalia_04", "Juan4n", "c3ba", "JorDa"
 
nom1 = input("introdueix el teu identificador de usuari: " )
nom2= input("Torn-al a escriure, sisplau: ")

# comprovem que els noms son iguals 
if nom1 != nom2:
	print( "ERROR: els dos noms introduits no son iguals")

else:
	# comprovar que el nom escrit es valid 
	nom=nom1.isidentifier()
	if not nom:
		print ("El nom escrit no es valid, sisplau prova amb un altre")
	else:
		# mirar si el nom ja esta a la base de dades
		valid=nom1.count(llista_users)
		if valid > 0:
			print ("El nom ja esta en us, sisplau introueix-ne un altre")
		if valid == 0:
			print ("Benvingut")
			# finalment si el nom esta be, itroduir-lo a la base de dades.
			llista_users.insert(nom1)
			print (llista_users)


























