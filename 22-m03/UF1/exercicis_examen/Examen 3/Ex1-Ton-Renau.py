# Ton Renau Mirambell
# Exercici 1

"""
Exercici 1 (3 punts) - 
Realitza un programa que, donat un nombre enter, determinisi és capicua o no. 
Considerarem també capicues els nombres enters d'una xifra.
El programa ha d’imprimir un missatge per pantalla indicant si el nombre introduït per
l’usuari és cap i cua o no. 
Es poden introduir tant nombres negatius com positius.

Per exemple: El nombre -14341 el considerarem capicua.
"""

print ("Cap i Cua")

# 1 especificaio de entrada

# Joc de proves 
# Entrada			Sortida
# 8 				Si, es capicua
# -8 				si, es capicua
# 49854				NO, no es capicua
# 4554				Si, es capicua 
# -14341			Si, es capicua 

"""
# 123321 --> 	1		2		3		3		2		1
# posicio		0		1	 	2		3		4		5	
#				len -6	len -5	len -4	len -3	len	-2	len -1
"""
num = input("Intrdueix el valor numeric: ")
 
palindromic = True

for i in range (len(num)):
	if not num[i] == num[len(num)-1 -i]: 
		palindromic = False
		
if palindromic:
	print ("El numero es cap i cua" )
else:
	print ("El numero no es cap i cua" ) 



