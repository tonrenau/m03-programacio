# Ton Renau Mirambell
# Exercici 3

"""
Exercici 3 (4 punts) - 
Realitza un programa que llegeix un nombre enter positiu n i,seguidament, 
llegeix n nombres enters positius i n’escriu el seu producte. 
No es pot realitzar cap multiplicació ni divisió en tot el programa.
De manera que els operadors *, /, // i % queden completament prohibits, 
així com queda prohibit qualsevol mètode o funció que permeti calcular productes i/o divisions.
Tampoc es poden usar potències ni arrels.

Observa que, per exemple, 3 · 5 = 3 + 3 + 3 + 3 + 3
"""

print ("Escriu el producte")
# 2 especificacions de entrada int, la "n" que sera el nombre ha descomposar i la "i" sera per tantes vegades 
#
# joc de proves 
# Entrada 			Sortida
# n = 3
# i = 5, i=2, i=3 	30				
# 
# n = 2
# i = 2,i=4			8


n = int(input("Introdueix el total de valors de nombres que vols multiplicar: "))

suma_actual = 1

for i in range (n):
	suma = suma_actual
	a = int(input("intodueix el valor que vols multiplicar: "))
	
	for i in range (a -1):
		suma_actual += suma

print("Resultat = ",suma_actual)

