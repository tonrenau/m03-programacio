"""
Equacions de segon grau
Versió 1:
Acaba de resoldre el programa que resol equacions de segon grau que vam
començar a l’activitat 5. Aquí tens el joc de proves:
Especificacions d’entrada: 3 nombres float a, b i c, corresponents als coeficients de
l’equació ax² + bx + c = 0. L’única restricció és que a ha de ser diferent de zero.
"""

# ax**2 + bx +c = 0 
# si a = 0 -> bx +c =0 -> x = -c/b
	# b = 0 -> c != 0 (infinites solucions) o c = 0 (cap colucio) 
# si a != 0 -> (-b +- (b**2 -4ac)**(1/2) )/2a
	# discriminant 
		# <0 -> No x
		# =0 -> x 
		# >0 -> x1, x2 



a = float(input("a="))
b = float(input("b="))
c = float(input("c="))

if a == 0:
	if b == 0:
		if c == 0:
			print ("infinites sol.")
			
		else:
			print ("cap sol.")
	
	else:
		x = -c/b
		print( "x=", x)
	
	
if a != 0:
	disc = b**2 -4*a*c
	
	if disc < 0:
		print ("cap solucio")
		
	elif disc == 0:
		x = -b / (2*a)
		print ("x=", x)
		
	else:
		x1 = (-b + (disc)**(1/2) )/ (2*a)
		x2 = (-b - (disc)**(1/2) )/ (2*a)
		print( "x1=",x1, "x2=", x2)
