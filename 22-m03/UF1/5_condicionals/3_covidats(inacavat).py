"""
Convidats
Recordeu el problema que tenia en una activitat anterior:
Em vull casar la setmana vinent i tinc un problema amb les taules i els convidats. He
pensat en convidar 431 persones i m'agradaria fer taules de 12 persones. Com que
els informàtics som molt metòdics i frikis, vull que a cada taula hi hagi exactament
12 convidats excepte possiblement en una taula, on hi posaré els convidats que
'sobrin'.
Vull fer un programa que em llegeixi el nombre de convidats, em digui les taules que
necessitaré i de quantes persones serà la 'taula diferent'.
Un alumne havia fet el següent joc de proves, i va tenir problemes perquè no
aconseguia verificar-lo només amb estructura seqüencial. S’ha posat molt content
perquè ara si que pot fer un programa que s’adeqüi a aquest joc de proves.
EE: nombres enters >= 2 (els nuvis com a mínim)
Entrada Sortida
convidats 		Taules 				Persones a la taula diferent
	38 				4 					2
	431 			36 					11
	6 				1 					6
	24 				s2 					0
Fes un programa que concordi amb aquest joc de proves.
"""

convidats = int(input("numero de convidats:"))

taules = convidats // 12 

taules_diff = convidats % 12 

if taules_diff > 0 :
	taules_t = taules +1
	print (taules_t) 
	
print (taules) 
