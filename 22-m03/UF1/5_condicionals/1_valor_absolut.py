"""
Valor absolut
Fes un programa que llegeixi un nombre i en mostri el valor absolut.
"""
# Ex 1 condicionals 
# 1 especificacio d'entrada, on sera un valor real
# Joc de proves
# 			Entrada 		Sortida
#Ex1:			3			3
#Ex2:			-3			3

valor = float(input("valor: "))

if valor < 0:
    valor = -valor
print ("EL teu valor final es",valor)