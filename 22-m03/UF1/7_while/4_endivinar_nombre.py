"""
Endevinar un nombre
Com a administradors farem pocs programes interactius, però aquest n’és una
excepció.
L’ordinador ‘pensarà’ un nombre entre el 1 i el 100 i l’usuari haurà d’endevinar el
nombre amb les pistes que li donarà el programa, que simplement dirà si el nombre
llegit és major o menor fins que l’usuari l’endevini.
Per a fer aquest programa utilitzarem una funció del mòdul random que es diu
randomint. Ens ajudarem de la documentació online per buscar la funció que
necessitem
"""

# a = 3**4 		== 		a = pow(3,4) --> pow es una funcio 
# em de utilizar la funcio randomint 

import random
a = random.randint(1,10)

n = 0 

while n != a:
	n = int(input("intodueix un nombre enter: "))
	if n == a:
		n = a
		print ("Macagundeu mas pillat")
	elif n < a:
		print ("subee subee")
	elif n > a:
		print ( "bajaaa")
	


