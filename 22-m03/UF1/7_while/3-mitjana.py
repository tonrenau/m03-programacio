"""
Mitjana de nombres llegits
Fes un programa que calculi la mitjana dels nombres positius llegits.
Està clar que aquest programa ofereix una lectura ambigua, i que cal determinar què volem
fer a través d’un joc de proves.
"""

"""
Versió 1:
LLegir nombres positius (enters o float, com decidim) fins que n’introduïm un de negatiu. En
aquell moment calculem la mitjana dels que hem entrat.
Exemple:
Entrada 		 Sortida
5, 3, 4, -5 		4
"""

num = 0
div = 0
sum_num = 0
while num >= 0: 
	
	a = float(input(" a = "))
	
	if a < 0:
		mitjana = sum_num / div 
	
	sum_num += num 
	div += 1
print (mitjana)

"""
Versió 2:
LLegirm una quantitat prefixada de nombres (amb una constant) i calculem la mitjana
només dels positius
Exemple per a N = 5:

5, 3, 4, -5, 8  		5
"""
n = int(input("quants valors vols: "))

cont = 0 
sum_num = 0
while cont < n:
	num = int(input("introdueix el valor: " )) 
	sum_num += num
	
	cont += 1
	
mitjana = sum_num / n 

print (mitjana) 



"""
Versió 3:
Igual que l’anterior, però el primer nombre introduït ens indica la quantitat de nombres que
llegirem.
Exemple:
5, 5, 3, 4, -5, 8 5
"""
