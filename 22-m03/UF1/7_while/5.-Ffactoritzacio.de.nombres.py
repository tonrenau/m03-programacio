"""
Factorial d’un nombre
Fer un programa que llegeixi un nombre positiu i en calculi el factorial.
El factorial d’un nombre es denota per n!:
Sabent que :
n! = = 1 · 2 · 3 · . . . · (n − 1) · n
i que
0! = 1
Fes un programa que demani el valor de n i mostri per pantalla el valor de n!
""" 

num = int(input("numero a factoritzar: "))

n = 1

while num > 1 :
	n += 1
	if num % n == 0:
		print (n) 
		num = num // n 
		n = 1
	
print ('1')
