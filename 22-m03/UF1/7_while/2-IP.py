"""
IPs
Fes un programa que llegeixi 4 nombres enters i determini si son els 4 camps que
conformen una ip vàlida.
fes dues versions d’aquest programa corresponents als següents exemples (son exemples,
no és un joc de proves. Abans de fer el programa, com sempre, cal que facis el joc de
pproves).
"""
"""
Versió 1:
Escriure una expressió booleana que em digui si el 4 camps que llegim d’una adreça ip
corresponen a una adreça vàlida o no.
Exemple:
Entrada: Sortida
192 168 0 1 True
2 -3 34 729 False
"""
print (" introdueix les valors de cada bit de 0-255 ") 
cont = 0
IP_valida = True
while cont < 4:
	IP = int(input("valor de la IP: "))
	if not (IP >= 0 and IP <= 255) :
		IP_valida = False
		
	cont +=1
	
if IP_valida == True:
	print (" IP valida ")
	
else: 
	print("IP no valida")
	
	cont += 1
	
"""
Versió 2 i 3:
Fer un programa que faci el mateix que l’expressió booleana anterior.
Primer fes que llegeixi i comprovi els 4 camps (sense bucle) i després utilitzant un bucle:
L’exemple és el mateix que el de l’expressió booleana
"""

# En un if, while --> un IP_valida =  IP_valida == True 

print (" introdueix les valors de cada bit de 0-255 ") 
cont = 0
IP_valida = True
while cont < 4 and IP_valida:
	IP = int(input("valor de la IP: "))
	if not (IP >= 0 and IP <= 255) :
		IP_valida = False
	
	cont += 1
	
if IP_valida:
	print (" IP valida ")
	
else: 
	print("IP no valida")
		



"""
Versió 4:
Fes un programa que ens digui si els 4 enters llegists corresponen a una ip vàlida, seguint
aquest exemple
Exemple:
Entrada: Sortida
2 -3 False
"""



