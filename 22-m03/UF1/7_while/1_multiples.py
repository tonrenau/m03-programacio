"""
Múltiples
Fes els següents programes:
1. Escriu els múltiples de dos menors o iguals a 100
"""


num = 2

while num < 100: 
	
	num = num + 2 
	print (num) 
	
"""
2. Escriu els 10 primers múltiples de dos
"""
num = 2

while num < 10: 
	
	num = num + 2 
	print (num) 


"""

3. Escriu els n primers múltiples de m. n i m són dades que llegeix el programa.
"""


multiple = int(input(" n = " )
limit = int(input(" m = " )
n = 1 

while n < limit:
	if n % multiple == 0:
		print (n)
	
	n += 1
	

"""
4. Sumar els múltiples de 2 menors de 100
"""
num = 2 
while num < 100:
	num+=2 
	print (num)




"""


5. Sumar las potències de n menores de 100
"""
base = int(input("base = "))
lim = int(input("lim = "))

n = 1
suma = 0 
potencia = base

while potencia < lim :
	suma += potencia 
	n +=1
	potencia = base**n
	print(suma)
	

"""
Mulptiples de 2 mes petits a 'n'
"""
n = int(input())
i = 2
while i < n:
	multiple = False
	if i % 2 == 0:
		multiple = True
		
	if multiple:
		print(i)
		
	i += 1
