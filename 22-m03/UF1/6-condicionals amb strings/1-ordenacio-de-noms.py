"""
El programa llegeix dos noms I els imprimeix per pantalla
ordenats en ordre alfabètic ascendent.
"""

# 2 especificacions de entrada on seran dos noms
"""
joc de proves 
				Entrada 			sortida
				Nom1: Ton
				Nom2: Joan 			Ton, Joan
				
				Nom1: Roc
				Nom2: Renau			Renau, Roc
"""

nom1 = str(input("nom1:"))
nom2 = str(input("nom2:"))

if nom1 > nom2:
	print (nom2, nom1)
	
else :

	print (nom1, nom2)
