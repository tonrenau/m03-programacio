"""
Programa 4 – El programa demana a l’usuari la introducció d’una contrasenya I
la seva repetició I llegeix les dues contrasenyes (com en el programa anterior).
Perque una contrasenya sigui vàlida s’han de reunir els següents criteris: Que les
dues contrasenyes coincideixin, que la contrasenya tingui més de 8 caràcters I
que la contrasenya comenci amb una lletra majúscula.
El programa comprova, en primer lloc, que les dues contrasenyes
coincideixin. Si no ho fan, el programa mostra el missatge “Les dues
contrasenyes no corresponen” per pantalla.
Si les dues contrasenyes coincideixen, el programa comprova que la
contrasenya tingui més de 8 caràcters. Si la contrasenya no compleix aquesta
condició, el programa mostra el missatge “La contrasenya és massa curta” per
pantalla.
Si les dues contrasenyes coincideixen I tenen més de 8 caràcters, el
programa comprova que la contrasenya comenci amb una lletra majúscula. Si la
contrasenya no compleix aquesta condició, el programa mostra el missatge “La
contrasenya no comença amb una contrasenya majúscula” per pantalla.
Si es compleixen totes aquestes tres condicions, el programa mostra el
missatge “Contrasenya vàlida” per pantalla.
"""


# 2 entrades que correspondran a cada cop que fiques la contrasenya
# REQUISITOS: 	Lletra MAYUS. al pricipi
#				8 caracters de contra
#				

"""
Joc de proves 			Entrada			Sortida	
						Bobo-bobo
						Bobo-bobo		contrasenya valida
						
						Bobo-bobo
						bobo	 		contrasenya invalida
						
"""

pwd1 = input("Introduce the pasword: ")

if len (pwd1) >= 8 and (pwd1[0] >= 'A' and pwd1[0] <= 'Z'):

	pwd2 = input("please repeat the pasword: ")
	
	if pwd1 == pwd2: 
		print("Succeful your pasword is save")
	else: 
		print("The pasword isn't the same, please repeat")
else: 
	print("The pasword isn't valid, please write another time")





