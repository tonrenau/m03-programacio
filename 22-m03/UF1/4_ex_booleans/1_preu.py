""" 
Exercicis booleanes 
EX PREU

Escriu una expressió booleana que determini que el valor d’una variable preu està
entre els 100 i els 150 euros, ambdós inclosos.
""" 

# 1 especificacio d'entrada
# joc de proves
#
# 						Entrada				Sortida	
# 						101					True
#						59 					False

a = int(input("valor del numero a avaluar:" ))

interval = 100 <= a <= 150

print (interval)
