"""
Siguin les variables enteres h, m i s. Escriu una expressió booleana que sigui certa
quan aquestes variables representin uns valors compatibles amb les hores, minuts i
segons d’un dia i sigui falsa en cas contrari.
"""

#Joc de proves:
#Si es una hora real = True si no = False

# 			Entrada: 			Soritda:
# 			12:30:25 			True
#
# 			27:65:85 			False

hores = int(input())
minuts = int(input())
segons = int(input())

hores_diaries = 00 <= hores <= 23 and 00 <= minuts <= 59 and 00 <= segons <= 59

print(hores_diaries)
