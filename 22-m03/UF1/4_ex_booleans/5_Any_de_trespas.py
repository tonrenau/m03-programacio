"""
Any de traspàs
Determina si un número enter positiu n representa un any de traspàs. Un any és de
traspàs si és múltiple de 4, excloent-ne els múltiples de 100 que no ho són de 400;
per exemple 1964, 2004 i 2400 són de traspàs, però 1977 i 2100 no.
"""
n = int(input("year to value:"))

any_traspas = (n % 4 == 0) and not ((n % 100 == 0 ) and (n % 400 != 0))

if any_traspas == True:
    print (any_traspas,",",n,"is a leap year")
else:
    print (any_traspas,",",n,"isn't a leap yes") 
