"""
Siguin a, b i c variables enteres i positives (i majors que zero). Escriu condicions
booleanes per als següents casos:
○ a és múltiple de b
○ a és divisor de b o de c
○ a, b i c son iguals entre si
○ a, b i c son tots diferents entre si
Repàs de conceptes de matemàtiques:
● El 8 és múltiple del 4 perquè si fem la divisió entera de 8 entre 4 el residu és
0.
● El 4 és divisor del 8 perquè si fem la divisió entera de 8 entre 4 el residu és 0.
● El 8 és divisible per 4 perquè si fem la divisió entera de 8 entre 4 el residu és
0.
"""

# 3 especificacions de entrada

# Joc de proves 
#			ENTRADA			SORTIDA
#			a=4 			False 
#			b=2			True	
#			c=1 			False	
#						True


a = int(input("a="))
b = int(input("b="))
c = int(input("c="))

multiple = a % b == 0 
divisor = b % a == 0 or c % a == 0
iguals = a == b and b == c
diferents = a != b and a != c and b != c

print ( multiple)
print ( divisor)
print (iguals)
print( diferents)

