"""
Repartiment d’euros
Reparteix una quantitat d'euros en el nombre de bitllets i monedes corresponents,
tenint en compte un algoritme que ens asseguri el menor nombre de
bitllets/monedes (és a dir, si podem utilitzar un bitllet de 10€, no n’utilitzarem de
menors).
"""
# Ton Renau Mirambell
# Asix 
#
# Data: 25/10/2022
#
# Versió 1:
# 
# 1 especificacio d'entrada float 
#
# Joc de proves:
# 
# 			Entrada			Sortida
# Execucio 1: 		10 euros 		10 euros	
# Ex2:			22 euros 		20 + 2 euros
# Ex3: 			3,22 euros		2 +1 +0.2 + 0.02 euros 

euros = float(input("euros:"))

canvi_50 = euros // 50
canvi_20 = euros //20  
canvi_10 = euros //10
canvi_5 = euros //5
canvi_2 = euros //2 
canvi_1 =euros //1
canvi_05 = euros //0.5 
canvi_02 = euros //0.2
canvi_01 = euros //0.1
canvi_005 =euros // 0.05
canvi_002 = euros // 0.02
canvi_001 = euros // 0.01

print ("billets de 50:",canvi_50)
print ("billets de 20:",canvi_20)
print ("monedes de 1:", canvi_1)
print ("monedes de 0.2", canvi_02)
print ("monedes de 0.02", canvi_002) 
